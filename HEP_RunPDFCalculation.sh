#!/bin/bash

#PBS -q hep_short
#PBS -N ChrisPDFCalculation_job
#PBS -l nodes=1:ppn=1
#PBS -l walltime=04:00:00
#PBS -M Christian.Weber@yale.edu
#PBS -m abe 
##PBS -I
##PBS -X

cd /home/hep/baker/ctw24/makePDFs_forCombinedPT

root -b -l -q  /home/hep/baker/ctw24/makePDFs_forCombinedPT/data/FullNtuple27_36M.root runProcess.C

## root -b -l -q  /home/hep/baker/ctw24/makePDFs_forCombinedPT/data/MidNtuple29_70k_pileup.root runProcess.C

## root -b -l -q  /home/hep/baker/ctw24/makePDFs_forCombinedPT/data/smallNtuple27_2k.root runProcess.C


#cd $PBS_O_WORKDIR


## qsub -X -I -q hep_short -l walltime=4:00:00

## qsub HEP_RunPDFCalculation.sh
//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Mar  7 11:27:17 2017 by ROOT version 5.30/01
// from TTree tree/tree
// found on file: data/mc15_13TeV_Electrons_2000_events.root
//////////////////////////////////////////////////////////

#ifndef calculatePDF_selector_h
#define calculatePDF_selector_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>

// function for the EP combination
#include "functions/CombinedPT_manager.h"

class calculatePDF_selector : public TSelector {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain

   // Declaration of leaf types
   Int_t           EventNumber;
   Double_t        eTrackPt;
   Double_t        eCaloE;
   Double_t        eTruePt;
   Double_t        eTrackEta;
   Double_t        eCaloEta;
   Double_t        eLastMeasP;
   Double_t        eFirstMeasP;
   Double_t        ePerigeeP;
   Int_t           truthTypeOfParticle;

   // List of branches
   TBranch        *b_EventNumber;           //!
   TBranch        *b_eTrackPt;              //!
   TBranch        *b_eCaloE;                //!
   TBranch        *b_eTruePt;               //!
   TBranch        *b_eTrackEta;             //!
   TBranch        *b_eCaloEta;              //!
   TBranch        *b_eLastMeasP;            //!
   TBranch        *b_eFirstMeasP;           //!
   TBranch        *b_ePerigeeP;             //!
   TBranch        *b_truthTypeOfParticle;   //!




TBranch *b_eventNumber;                      //!
TBranch *b_electronTruePt;                   //!
TBranch *b_electronTrueEta;                  //!
TBranch *b_electronEta;                      //!
TBranch *b_electronPt;                       //!
TBranch *b_electronE;                        //!
TBranch *b_electronE_calibrated;             //!
TBranch *b_electronCaloE;                    //!
TBranch *b_electronCaloEta;                  //!
TBranch *b_electronTrackerPt;                //!
TBranch *b_electronTrackerLastMeasuredP;     //!
TBranch *b_electronTrackerFirstMeasuredP;    //!
TBranch *b_electronTrackerPerigeeP;          //!
TBranch *b_particleTruthType;                //!






   calculatePDF_selector(TTree * /*tree*/ =0) { }
   virtual ~calculatePDF_selector() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   ClassDef(calculatePDF_selector,0);
};

#endif

#ifdef calculatePDF_selector_cxx
void calculatePDF_selector::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("eTrackPt", &eTrackPt, &b_electronTrackerPt);

   //fChain->SetBranchAddress("eE", &eCaloE, &b_electronE);
   // alternatively, if I wanna use the explicitly re-calibrated energy: 
   fChain->SetBranchAddress("eE_calibrated", &eCaloE, &b_electronE_calibrated); 
   
   fChain->SetBranchAddress("eTruePt", &eTruePt, &b_electronTruePt);
   fChain->SetBranchAddress("eEta", &eTrackEta, &b_electronEta);
   fChain->SetBranchAddress("eCaloEta", &eCaloEta, &b_electronCaloEta);
   fChain->SetBranchAddress("eLastMeasP", &eLastMeasP, &b_electronTrackerLastMeasuredP);
   fChain->SetBranchAddress("eFirstMeasP", &eFirstMeasP, &b_electronTrackerFirstMeasuredP);
   fChain->SetBranchAddress("ePerigeeP", &ePerigeeP, &b_electronTrackerPerigeeP);
   fChain->SetBranchAddress("truthTypeOfParticle", &truthTypeOfParticle, &b_particleTruthType);



/* Full list of branches from the EP_nTupleMaker

    tree->Branch("EventNumber", &eventNumber);

    tree->Branch("eTruePt", &electronTruePt);     // (trueParticlePtr)->pt();
    tree->Branch("eTrueEta", &electronTrueEta);   // (trueParticlePtr)->eta();

    tree->Branch("eEta", &electronEta); 
    tree->Branch("ePt", &electronPt);
    tree->Branch("eE", &electronE);
    tree->Branch("eE_calibrated", &electronE_calibrated);

    tree->Branch("eCaloE", &electronCaloE);
    tree->Branch("eCaloEta", &electronCaloEta);

    tree->Branch("eTrackPt", &electronTrackerPt);

    tree->Branch("eLastMeasP", &electronTrackerLastMeasuredP);
    tree->Branch("eFirstMeasP", &electronTrackerFirstMeasuredP);
    tree->Branch("ePerigeeP", &electronTrackerPerigeeP);

    tree->Branch("truthTypeOfParticle", &particleTruthType);

*/









}

Bool_t calculatePDF_selector::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}



#endif // #ifdef calculatePDF_selector_cxx

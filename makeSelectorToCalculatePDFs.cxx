// uses Roots TSelector class to makes an analysis skeleton
// I will use this skeleton to derive the PDFs for the CombinedPT code
// Reference for the TSelector class: https://root.cern.ch/developing-tselector

{
	// get a pointer to the file containing the data
	TFile* myElectronTFilePointer = TFile::Open("data/mc15_13TeV_Electrons_2000_events.root","READ");
	// get a pointer to the TTree carrying the data
	TTree *myElectronDataTreePointer = (TTree *) myElectronTFilePointer->Get("tree");
	// make the analysis skeleton using 
	myElectronDataTreePointer->MakeSelector("calculatePDF_selector");
}
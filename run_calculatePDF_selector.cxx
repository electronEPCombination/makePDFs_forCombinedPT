// these commands should run the calculatePDF_selector code

{	//to load the data of interest
	//TFile electronData("data/mc15_13TeV_Electrons_2000_events.root","READ");
	// or load the data while starting root: 
	//root -l data/mc15_13TeV_Electrons_2000_events.root
	// or run in batch mode: 
	root -b -l data/mc15_13TeV_Electrons_2k_events.root
	// or in batch with the full data set
	// root -b -l data/BIGntuple.root

// (mostly?) complete mc 20.7 dataset  
	tree->Process("calculatePDF_selector.C");
}
## Let's try to make a programm that gets me all root files in a directory, opens them, and combines a histograms of much choice that's in all of the root files

# to run this script: 					python runTChainProcess.py -dataset /afs/cern.ch/user/c/chweber/Downloads/temp_nTuples/user.chweber
# or while already running python:		execfile('runTChainProcess.py')


## get options by parsing command line arguments
def getopts(argv):
	opts = {}  # Empty dictionary to store key-value pairs.
	while argv:  # While there are arguments left to parse...
		if argv[0][0] == '-':  # Found a "-name value" pair.
			opts[argv[0]] = argv[1]  # Add key and value to the dictionary.
		argv = argv[1:]  # Reduce the argument list by copying it starting from index 1.
	return opts


# the part of the code that executes of I call this script directly
if __name__ == '__main__':

	from sys import argv # to parse command line options
	from sys import exit # to leave the programm early

	from os import listdir # to list the contents of a directory
	
	import ROOT	# to do all the ROOT stuff

	myargs = getopts(argv)
	if '-dataset' in myargs:  # Example usage.
	    print("Using data in: " + myargs['-dataset'])
	
	else:
		exit("No data given. Call the program like: \t python runTChainProcess.py -dataset \"location of data folder\"")

	# where the files of interest reside
	fileDir = myargs['-dataset']

	# add a backslash to the filedir for later, in case there isn't one yet
	if not fileDir.endswith("/"): fileDir = fileDir + "/"

	# get content of the directory
	dirContent = listdir(fileDir)


	# create a ROOT TChain, where we are chaining TTrees 'tree' from the root files together
	myRootFileChain = ROOT.TChain("tree")

	# attach all of the files in the directory to the TChain (Assume for now that there are only .root files in the directory)
	for n in range(0,len(dirContent)):
		myRootFileChain.Add(fileDir+dirContent[n])

	# process the TTrees in the TChain with my selector class
	myRootFileChain.Process("calculatePDF_selector.C")
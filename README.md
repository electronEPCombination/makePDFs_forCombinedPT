To clone the git repository
Navigate to the directory where you want to put the code, then clone the project:

      git clone ssh://git@gitlab.cern.ch:7999/chweber/makePDFs_forCombinedPT

Overview

Calculate PDF parameters from a root TTree where each event corresponds to an MC Electron (calculatePDF_selector.C)
And make plots of the underlying fits, as well as the effect and efficacy of the combination procedure (combinationEfficacy.py, plotFitPulls.py)
. And make 

Make the PDFs

This is done with the 'calculatePDF_selector.C'. As the name suggests it inherits from the selector class, and it is a root programm.

If you want to run it on a signle file, use the 'runProcess.C' macro, by attaching the appropriate .root file to it:
root -b -l -q  dataForCombination.root runProcess.C
Where runProcess.C defines the name of the TTree in dataForCombination.root as 'tree'.

To run on multiple root files spanning a single dataset use the runTChainProcess.py PyRoot script:

python runTChainProcess.py -dataset "Location of folder that contains the root files with TTree 'tree' "



The TTree is expected to have the following branches:
EventNumber
eTrackPt - electron pT as measured by the tracker
eE - electron energy as measured by the calorimeter
eE_calibrated - electron energy as measured by the calorimeter, explicitly recalibrated
eTruePt - true electron pT
eEta - electron eta as measured by the tracker
eCaloEta - electron eta as measured by the calorimeter
eLastMeasP - electron momentum at the least measured tracker point (As measured by the tracker)
eFirstMeasP - electron momentum at the first measured tracker point (As measured by the tracker)
ePerigeeP - electron momentum at closest approach to the beam
truthTypeOfParticle - truth type of the particle ( UnknownElectron = 1,  IsoElectron =  2,  NonIsoElectron = 3,  BkgElectron = 4,)


'calculatePDF_selector.C' creates the following files:

outAggregatedPlots.root 	- An aggregation of fit plots, and THists of pT/pT_true, including pT_combined/pT_true
outputPlots.pdf 			- An overview of of the pT/pT_true distributions for pT = pT_combined, pT_calo, and pT_tracker
outCrystalBallParameters.c  - A file containing the parameters for the likelihood functions for the different phase space regions.

Pulls, efficacy and effect

Term definition: 
efficacy - is here the ability to improve the electron pT measurement via combination, in other words, how much closer the electron pT is to the true value, if we do the combination
effect - is the amount of change in electron pT of the whole electron ensamble, i.e. this includes electron where we decide to do the combination, and those where we decide to not do so

So the combination can have a large efficacy, but a small effect, for example when where there's only a very small set of cases where we do the combination, but where it improves the combination a lot.


The phython programm 'combinationEfficacy.py' provides overviews of combination efficacy and effect, and can also output plots of the likelihood fits and associated pulls.
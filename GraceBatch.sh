#!/bin/bash

#cd /home/hep/baker/ctw24/makePDFs_forCombinedPT

## copy programm related files
cd /home/hep/baker/ctw24/makePDFs_forCombinedPT

#cp -r /home/hep/baker/ctw24/makePDFs_forCombinedPT/* . #copy the program and local data
#cp /afs/cern.ch/user/c/chweber/gitArea/makePDFs_forCombinedPT/data/* .
#cp /home/hep/baker/ctw24/dataForMakePDFs/* ./data/ # copy the data from the workspace

#cd /afs/cern.ch/user/c/chweber/gitArea/makePDFs_forCombinedPT

#root -b -l -q  data/smallNtuple27_2k.root runProcess.C
#root -b -l -q  data/FullNtuple29_38M_pileup.root runProcess.C 
root -b -l -q  data/FullNtuple27_36M.root runProcess.C 

#source ~/cmthome/setup.sh -tag=14.2.10,32

cp outAggregatedPlots.root 	/home/hep/baker/ctw24/makePDFs_forCombinedPT/batchOut
cp outputPlots.pdf 			/home/hep/baker/ctw24/makePDFs_forCombinedPT/batchOut
cp outCrystalBallParameters.c /home/hep/baker/ctw24/makePDFs_forCombinedPT/batchOut
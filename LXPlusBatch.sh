#!/bin/bash

## things??
export RFIO_USE_CASTOR_V2=YES
export STAGE_HOST=castoratlas
export STAGE_SVCCLASS=atlt3
export CURRENT_DIR=`pwd`

## copy programm related files
cp -r /afs/cern.ch/user/c/chweber/EPCombination/makePDFs_forCombinedPT/* . #copy the program and local data
#cp /afs/cern.ch/user/c/chweber/EPCombination/makePDFs_forCombinedPT/data/* .
#cp /afs/cern.ch/user/c/chweber/myWorkspace/data/dataForPDFcalculation/mc16a_electrons_noPileup_FullSample_40M.root ./data/ # copy the data from the workspace

#cd /afs/cern.ch/user/c/chweber/EPCombination/makePDFs_forCombinedPT

#root -b -l -q  data/smallNtuple27_2k.root runProcess.C
#root -b -l -q  data/FullNtuple29_38M_pileup.root runProcess.C 

## xAOD MC16 pileup electron sample
root -b -l -q  /afs/cern.ch/user/c/chweber/myWorkspace/data/nTuple_EPCombination_xAOD_pileup/user.chweber.mc16_13TeV.423000.ParticleGun_single_electron_egammaET.deriv.xAOD.EpCombination.nTuple.pileup.02.423000_myOutput.root  runProcess.C  

## xAOD MC16 single electron sample
#root -b -l -q  /afs/cern.ch/user/c/chweber/myWorkspace/data/nTuple_EPCombination_xAOD/user.chweber.mc16_13TeV.423000.ParticleGun_single_electron_egammaET.deriv.xAOD.EpCombination.nTuple.05_myOutput.root runProcess.C 




#source ~/cmthome/setup.sh -tag=14.2.10,32

rfcp outAggregatedPlots.root 	/afs/cern.ch/user/c/chweber/EPCombination/makePDFs_forCombinedPT/batchOut
rfcp outputPlots.pdf 			/afs/cern.ch/user/c/chweber/EPCombination/makePDFs_forCombinedPT/batchOut
rfcp outCrystalBallParameters.c /afs/cern.ch/user/c/chweber/EPCombination/makePDFs_forCombinedPT/batchOut

#  bsub -R "pool>5000" -q 8nh LXPlusBatch.sh


#ifndef __COMBINEDPT_CALCULATOR_C__
#define __COMBINEDPT_CALCULATOR_C__
#include "functions/CombinedPT_calculator.h"
#include <TError.h>

//for testing purposes
#include <iostream> // for cin, cout

/*
  CombinedPT_calculator::CombinedPT_calculator(Float_t meanCluster, Float_t meanTrack, Float_t sigmaCluster, Float_t sigmaTrack, Float_t aCluster, Float_t aTrack, Float_t nCluster, Float_t nTrack, bool useDebug) : 
    m_useDebugMessages(useDebug)
{
    
  max = 0.;
  min = 0.;

  mean_cluster  = new RooRealVar("mean_cluster","mean of gaussian",meanCluster);
  mean_track    = new RooRealVar("mean_track",  "mean of gaussian",meanTrack);
  sigma_cluster = new RooRealVar("sigma_cluster","width of gaussian",sigmaCluster); 
  sigma_track   = new RooRealVar("sigma_track", "width of gaussian",sigmaTrack); 
  a_cluster     = new RooRealVar("a_cluster","a_cluster",aCluster);
  a_track       = new RooRealVar("a_track",     "a",aTrack);
  n_cluster     = new RooRealVar("n_cluster","n_cluster",nCluster);
  n_track       = new RooRealVar("n_track","n_track",nTrack);
  p_track       = new RooRealVar("p_track",   "p_track",   1.0);
  p_cluster     = new RooRealVar("p_cluster", "p_cluster", 1.0);
  x             = new RooRealVar("x","x", 1.0, 0., 1000000.);
  A             = new RooFormulaVar("A","A","@0/@1",RooArgList(*p_track,  *x));
  B             = new RooFormulaVar("B","B","@0/@1",RooArgList(*p_cluster,*x));
  CBtrack       = new RooCBShape("CBtrack",   "crystalBall PDF", *A, *mean_track,   *sigma_track,   *a_track,   *n_track) ; 
  CBcluster     = new RooCBShape("CBcluster", "CBcluster",       *B, *mean_cluster, *sigma_cluster, *a_cluster, *n_cluster);
  product       = new RooProdPdf("product", "CBtrack*CBcluster", RooArgSet(*CBtrack, *CBcluster));
  prodlog       = new RooFormulaVar("prodlog", "prodlog", "-log(@0)", *product);
  m             = new RooMinuit(*prodlog);


  //Silence output messages.
  if (!m_useDebugMessages) {

    m->setPrintLevel(-10000);
    m->setWarnLevel(-10000);

    RooMsgService::instance().setGlobalKillBelow(RooFit::FATAL) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::NumIntegration) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::Fitting) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::Integration) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::LinkStateMgmt) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::Caching) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::Optimization) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::ObjectHandling) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::Tracing) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::Contents) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::DataHandling) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::Minimization) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::InputArguments) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::Eval);
    RooMsgService::instance().getStream(1).removeTopic(RooFit::Plotting) ;

  }//Silence output messages.

  
} // CombinedPT_calculator::CombinedPT_calculator
*/


  CombinedPT_calculator::CombinedPT_calculator(Float_t meanCluster, Float_t meanTrack, Float_t sigmaCluster, Float_t sigmaTrack, Float_t aCluster, Float_t aTrack, Float_t nCluster, Float_t nTrack, 
    Float_t meanGaussCalo, Float_t meanGaussTrack, Float_t sigmaGaussCalo, Float_t sigmaGaussTrack, Float_t caloCBWeight, Float_t trackCBWeight, bool useDebug) : 
    m_useDebugMessages(useDebug)
{
    
  max = 0.;
  min = 0.;

  // Crystal Ball Parameters
  mean_cluster  = new RooRealVar("mean_cluster","mean of gaussian",meanCluster);
  mean_track    = new RooRealVar("mean_track",  "mean of gaussian",meanTrack);
  sigma_cluster = new RooRealVar("sigma_cluster","width of gaussian",sigmaCluster); 
  sigma_track   = new RooRealVar("sigma_track", "width of gaussian",sigmaTrack); 
  a_cluster     = new RooRealVar("a_cluster","a_cluster",aCluster);
  a_track       = new RooRealVar("a_track",     "a",aTrack);
  n_cluster     = new RooRealVar("n_cluster","n_cluster",nCluster);
  n_track       = new RooRealVar("n_track","n_track",nTrack);
  p_track       = new RooRealVar("p_track",   "p_track",   1.0);
  p_cluster     = new RooRealVar("p_cluster", "p_cluster", 1.0);

  // Gaussian Parameters meanGaussCalo, meanGaussTrack, sigmaGaussCalo, sigmaGaussTrack
  mean_gauss_cluster  = new RooRealVar("mean_gauss_cluster", "mean of gaussian",  meanGaussCalo);
  mean_gauss_track    = new RooRealVar("mean_gauss_track"  , "mean of gaussian",  meanGaussTrack);
  sigma_gauss_cluster  = new RooRealVar("sigma_gauss_cluster", "width of gaussian", sigmaGaussCalo); 
  sigma_gauss_track    = new RooRealVar("sigma_gauss_track"  , "width of gaussian", sigmaGaussTrack); 
  //parameters for the PDF sum
  weight_addPDF_cluster = new RooRealVar("weight_addPDF_cluster", "weight of CB in PDF sum for tracker", caloCBWeight);
  weight_addPDF_tracker = new RooRealVar("weight_addPDF_tracker", "weight of CB in PDF sum for calorimeter", trackCBWeight);

  x             = new RooRealVar("x","x", 1.0, 0., 1000000.); // independent variable, eventually what we will take as the best guess of the true pT: the combined pT
  A             = new RooFormulaVar("A","A","@0/@1",RooArgList(*p_track,  *x)); // pT_track / pT_combined
  B             = new RooFormulaVar("B","B","@0/@1",RooArgList(*p_cluster,*x)); // pT_calo  / pT_combined

  CBtrack       = new RooCBShape("CBtrack",   "crystalBall PDF", *A, *mean_track,   *sigma_track,   *a_track,   *n_track) ; 
  CBcluster     = new RooCBShape("CBcluster", "CBcluster",       *B, *mean_cluster, *sigma_cluster, *a_cluster, *n_cluster);

  GaussTrack       = new RooGaussian("GaussTrack",   "Gaussian PDF", *A, *mean_gauss_track,   *sigma_gauss_track  ); 
  GaussCluster     = new RooGaussian("GaussCluster", "Gaussian PDF", *B, *mean_gauss_cluster, *sigma_gauss_cluster);

  sumCBgaussTrack = new RooAddPdf("sumCBgaussTrack", "Track CB+Gauss", *CBtrack  , *GaussTrack  , *weight_addPDF_tracker);
  sumCBgaussCalo  = new RooAddPdf("sumCBgaussCalo" , "Calo CB+Gauss" , *CBcluster, *GaussCluster, *weight_addPDF_cluster);

  product       = new RooProdPdf("product", "CBtrack*CBcluster", RooArgSet(*sumCBgaussTrack, *sumCBgaussCalo));
  prodlog       = new RooFormulaVar("prodlog", "prodlog", "-log(@0)", *product);
  m             = new RooMinuit(*prodlog);


  //Silence output messages.
  if (!m_useDebugMessages) {

    m->setPrintLevel(-10000);
    m->setWarnLevel(-10000);

    RooMsgService::instance().setGlobalKillBelow(RooFit::FATAL) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::NumIntegration) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::Fitting) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::Integration) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::LinkStateMgmt) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::Caching) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::Optimization) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::ObjectHandling) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::Tracing) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::Contents) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::DataHandling) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::Minimization) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::InputArguments) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::Eval);
    RooMsgService::instance().getStream(1).removeTopic(RooFit::Plotting) ;

  }//Silence output messages.

  
} // CombinedPT_calculator::CombinedPT_calculator

CombinedPT_calculator::~CombinedPT_calculator()
{
  delete sigma_track;
  delete mean_track;
  delete a_track;
  delete n_track;
  delete sigma_cluster;
  delete mean_cluster;
  delete a_cluster;
  delete n_cluster;
  delete x;
  delete p_track;
  delete p_cluster;

  delete A;
  delete B;

  delete prodlog;
  delete product;

  delete m;

  delete CBtrack;
  delete CBcluster;
} // CombinedPT_calculator::~CombinedPT_calculator


void CombinedPT_calculator::Evaluate(double pT_track, double pT_cluster, double &ptCombined, double &ptError) {
 
  ptCombined=-999.;
  ptError=-999.;
  p_track->setVal(  pT_track);
  p_cluster->setVal(pT_cluster);

  if(pT_track >= pT_cluster) {
    
    max = 1.2 * pT_track;
    min = 0.8 * pT_cluster;
    
  } else {

    max = 1.2 * pT_cluster;
    min = 0.8 * pT_track;
  }


  double _val = (pT_track+pT_cluster)/2.;

 
  if (min >= x->getMax()) {
    
    x->setMax(max);
    x->setMin(min);
    
  } else {
    
    x->setMin(min);
    x->setMax(max);
  }


  x->setVal(_val);

  /* old section 
  //m->migrad();
  //m->hesse();
  //m->minos(RooArgSet(*x));
  */

  Int_t statusM=m->migrad();
  if (statusM!=0)
    {
      m->migrad();
      m->hesse();
      m->minos(RooArgSet(*x));
    }
  m->hesse();
  m->minos(RooArgSet(*x));

  // product->getParameters(*x)->Print("s");
  //RooFitResult* r = m->save() ;
    
  Float_t xmax = x->getVal(); //// This is the line that causes the std::string related error
  //Float_t xmax = x->getValV();  //// This does not, it appears to me that this is what we want, but I need to double check at some point

  Float_t CombinedPtError = x->getError();
  //Float_t CombinedPtErrorHigh= x->getErrorHi();
  //Float_t CombinedPtErrorLow= x->getErrorLo();
 
  //r->Print("v");
  ptCombined = xmax; // outcommented as a consequence of the line above
  ptError = CombinedPtError;

} // void CombinedPT_calculator::Evaluate


#endif

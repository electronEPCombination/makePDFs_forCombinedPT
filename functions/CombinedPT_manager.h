#ifndef __COMBINEDPT_MANAGER_H__
#define __COMBINEDPT_MANAGER_H__

#include "functions/CombinedPT_calculator.h"
#include <TError.h>
#include "RooMsgService.h"

using namespace RooFit;



class CombinedPT_manager : public TNamed {

 public:
  
  CombinedPT_manager(int mcPeriod, bool useDebug = false);
  ~CombinedPT_manager();

  void getCombinedPt(double pT_track, double pT_cluster, double eta_track, double dPoverPin,
       double &ptCombined, double &ptError);

  void UseGeV(bool useGeV = 1) { m_useGeV = useGeV;} 

private: 

  int  m_mcPeriod;
  bool m_useDebugMessages;

  CombinedPT_calculator* vLC_LB  ;
  CombinedPT_calculator* vLC_HB  ;
  CombinedPT_calculator* vLM_LB  ;
  CombinedPT_calculator* vLM_HB  ;
  CombinedPT_calculator* vLCr_LB ;
  CombinedPT_calculator* vLCr_HB ;
  CombinedPT_calculator* vLF_LB  ;
  CombinedPT_calculator* vLF_HB  ;
  CombinedPT_calculator* LC_LB   ;
  CombinedPT_calculator* LC_HB   ;
  CombinedPT_calculator* LM_LB   ;
  CombinedPT_calculator* LM_HB   ;
  CombinedPT_calculator* LCr_LB  ;
  CombinedPT_calculator* LCr_HB  ;
  CombinedPT_calculator* LF_LB   ;
  CombinedPT_calculator* LF_HB   ;
  CombinedPT_calculator* MC_LB   ;
  CombinedPT_calculator* MC_HB   ;
  CombinedPT_calculator* MM_LB   ;
  CombinedPT_calculator* MM_HB   ;
  CombinedPT_calculator* MCr_LB  ;
  CombinedPT_calculator* MCr_HB  ;
  CombinedPT_calculator* MF_LB   ;
  CombinedPT_calculator* MF_HB   ;
  CombinedPT_calculator* HC_LB   ;
  CombinedPT_calculator* HC_HB   ;
  CombinedPT_calculator* HM_LB   ;
  CombinedPT_calculator* HM_HB   ;
  CombinedPT_calculator* HCr_LB  ;
  CombinedPT_calculator* HCr_HB  ;
  CombinedPT_calculator* HF_LB   ;
  CombinedPT_calculator* HF_HB   ;
 
  bool m_useGeV; 



  
}; //class CombinedPT_manager

#endif

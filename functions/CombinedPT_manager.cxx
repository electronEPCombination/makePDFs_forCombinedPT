#ifndef __COMBINEDPT_MANAGER_C__
#define __COMBINEDPT_MANAGER_C__
#include "functions/CombinedPT_manager.h"
#include "functions/CombinedPT_calculator.h"
#include <TError.h>
#include "RooMsgService.h"




CombinedPT_manager::CombinedPT_manager(int mcPeriod, bool useDebug) :
  m_mcPeriod(mcPeriod),
  m_useDebugMessages(useDebug),
  m_useGeV (false)
{

  //Note: m_mcPeriod will correspond to values of:
  //MC11c: 0, MC11d: 1, MC12a: 2, MC12c: 3.

  //Temporarily removing options for MC11d, since PDFs are unavailable.
  //if (m_mcPeriod == 1) m_mcPeriod = 0;

  if (m_mcPeriod == 0) {
    std::cout << "CombinedPT_manager: Using MC11c PDFs " << std::endl;

    //PDF for mc11c 
    //                                  (meanCluster, meanTrack, sigmaCluster, sigmaTrack, aCluster, aTrack,   nCluster,  nTrack, bool useDebug)
    LC_LB  = new CombinedPT_calculator  (0.993253,    0.986926,  0.0361747,    0.0226043,  1.13905,  0.443767, 6.9476,    20,     m_useDebugMessages);
    LC_HB  = new CombinedPT_calculator  (0.981778,    0.982407,  0.038974,     0.0321124,  0.780896, 0.545763, 20,        20,     m_useDebugMessages);
    LM_LB  = new CombinedPT_calculator  (0.987091,    0.966356,  0.0515119,    0.035908,   0.768316, 0.477371, 20,        20,     m_useDebugMessages);
    LM_HB  = new CombinedPT_calculator  (0.955335,    0.970258,  0.0616836,    0.0419302,  0.797949, 0.474146, 20,        20,     m_useDebugMessages);
    LCr_LB = new CombinedPT_calculator  (0.925499,    0.918206,  0.1,          0.0617341,  0.910728, 0.497233, 20,        20,     m_useDebugMessages);
    LCr_HB = new CombinedPT_calculator  (0.9,         0.93109,   0.0996782,    0.0607543,  0.895867, 0.460083, 20,        20,     m_useDebugMessages);
    LF_LB  = new CombinedPT_calculator  (0.99123,     0.909876,  0.0391606,    0.0749811,  0.62363,  0.950758, 7.96918,   20,     m_useDebugMessages);
    LF_HB  = new CombinedPT_calculator  (0.968377,    0.9,       0.0548691,    0.0827742,  0.488835, 0.778343, 20,        20,     m_useDebugMessages);
    MC_LB  = new CombinedPT_calculator  (0.995504,    0.98259,   0.0231325,    0.025577,   1.25957,  0.695559, 5.40461,   20,     m_useDebugMessages);
    MC_HB  = new CombinedPT_calculator  (0.993343,    0.963307,  0.0225748,    0.047077,   0.734976, 1.22854,  19.9998,   20,     m_useDebugMessages);
    MM_LB  = new CombinedPT_calculator  (0.994362,    0.957864,  0.0337238,    0.0424026,  0.865559, 0.860805, 12.7774,   20,     m_useDebugMessages);
    MM_HB  = new CombinedPT_calculator  (0.984025,    0.958237,  0.0368382,    0.0506166,  0.714859, 0.809051, 20,        20,     m_useDebugMessages);
    MCr_LB = new CombinedPT_calculator  (0.949393,    0.91126,   0.0733242,    0.0633476,  0.79691,  0.737056, 20,        20,     m_useDebugMessages);
    MCr_HB = new CombinedPT_calculator  (0.923377,    0.903426,  0.0796809,    0.0770462,  0.783358, 0.767606, 19.9999,   20,     m_useDebugMessages);
    MF_LB  = new CombinedPT_calculator  (0.995425,    0.91695,   0.0274372,    0.0740894,  0.748407, 1.35909,  5.61919,   20,     m_useDebugMessages);
    MF_HB  = new CombinedPT_calculator  (0.981916,    0.9,       0.0428046,    0.0835605,  0.547581, 1.22682,  20,        20,     m_useDebugMessages);
    HC_LB  = new CombinedPT_calculator  (0.997316,    0.974115,  0.0147463,    0.0373739,  1.30308,  2.21872,  6.59544,   20,     m_useDebugMessages);
    HC_HB  = new CombinedPT_calculator  (0.995326,    0.956893,  0.0162229,    0.0646685,  1.12714,  2.88867,  7.17726,   20,     m_useDebugMessages);
    HM_LB  = new CombinedPT_calculator  (0.998742,    0.953567,  0.0204386,    0.05239,    1.0462,   2.3312,   8.05072,   20,     m_useDebugMessages);
    HM_HB  = new CombinedPT_calculator  (0.995932,    0.943037,  0.0224859,    0.0718727,  0.813301, 2.31486,  11.0556,   20,     m_useDebugMessages);
    HCr_LB = new CombinedPT_calculator  (0.979859,    0.922967,  0.0468676,    0.0638544,  0.612779, 0.991785, 20,        20,     m_useDebugMessages);
    HCr_HB = new CombinedPT_calculator  (0.966744,    0.9,       0.0521472,    0.0937,     0.579683, 1.37679,  20,        20,     m_useDebugMessages);
    HF_LB  = new CombinedPT_calculator  (0.99644,     0.917046,  0.0208129,    0.0852612,  1.15053,  3.3246,   4.01055,   20,     m_useDebugMessages);
    HF_HB  = new CombinedPT_calculator  (0.989546,    0.9,       0.0330047,    0.0984734,  0.801897, 2.60165,  8.30776,   20,     m_useDebugMessages);

  } //if (m_mcPeriod == 0)

  else if (m_mcPeriod == 1) {
    std::cout << "CombinedPT_manager: Using MC11d PDFs " << std::endl;
    //PDF for mc11c 
    //                                  (meanCluster, meanTrack, sigmaCluster, sigmaTrack, aCluster, aTrack,   nCluster,  nTrack, bool useDebug)
    LC_LB   = new CombinedPT_calculator (0.994033,0.988106,0.0350208,0.0204645,0.984496,0.393614,9.22303,20, m_useDebugMessages);
    LC_HB   = new CombinedPT_calculator (0.983324,0.982197,0.0370767,0.0309119,0.703697,0.499847,20,20, m_useDebugMessages);
    LM_LB   = new CombinedPT_calculator (0.983757,0.96749,0.0534241,0.0348368,0.880694,0.469051,20,20, m_useDebugMessages);
    LM_HB   = new CombinedPT_calculator (0.949333,0.969902,0.0657066,0.0401866,0.973398,0.465001,20,20, m_useDebugMessages);
    LCr_LB  = new CombinedPT_calculator (0.920106,0.910667,0.1,0.064133,0.955895,0.534078,20,20, m_useDebugMessages);
    LCr_HB  = new CombinedPT_calculator (0.9,0.919698,0.1,0.0683296,0.960628,0.535809,20,20, m_useDebugMessages);
    LF_LB   = new CombinedPT_calculator (0.985656,0.9,0.0407313,0.0801346,0.585811,1.05899,18.9524,20, m_useDebugMessages);
    LF_HB   = new CombinedPT_calculator (0.944788,0.9,0.0657626,0.0799084,0.64468,0.73583,20,20, m_useDebugMessages);
    MC_LB   = new CombinedPT_calculator (0.995604,0.982681,0.0233894,0.0251796,1.23161,0.681999,5.97581,20, m_useDebugMessages);
    MC_HB   = new CombinedPT_calculator (0.993168,0.968534,0.02273,0.042662,0.752928,0.975104,20,20, m_useDebugMessages);
    MM_LB   = new CombinedPT_calculator (0.994432,0.957159,0.0335869,0.0429373,0.904334,0.909777,20,20, m_useDebugMessages);
    MM_HB   = new CombinedPT_calculator (0.980339,0.954118,0.0394066,0.0535119,0.892404,0.913344,20,20, m_useDebugMessages);
    MCr_LB  = new CombinedPT_calculator (0.945659,0.910187,0.0715756,0.0663796,0.766194,0.737332,20,20, m_useDebugMessages);
    MCr_HB  = new CombinedPT_calculator (0.919839,0.9,0.0787451,0.0822078,0.836718,0.867519,20,20, m_useDebugMessages);
    MF_LB   = new CombinedPT_calculator (0.990601,0.909529,0.0304858,0.0802444,0.881035,1.68785,5.95566,20, m_useDebugMessages);
    MF_HB   = new CombinedPT_calculator (0.975627,0.9,0.0444759,0.0872979,0.615764,1.37827,20,20, m_useDebugMessages);
    HC_LB   = new CombinedPT_calculator (0.997417,0.973958,0.0148497,0.0372782,1.2936,2.20651,6.99788,6.46125, m_useDebugMessages);
    HC_HB   = new CombinedPT_calculator (0.995157,0.956589,0.0159476,0.0660837,1.07584,2.44105,9.48607,19.9998, m_useDebugMessages);
    HM_LB   = new CombinedPT_calculator (0.998979,0.954033,0.0202494,0.0523873,1.05974,2.38784,14.8701,9.54934, m_useDebugMessages);
    HM_HB   = new CombinedPT_calculator (0.993174,0.941414,0.0243645,0.0727002,1.07803,3.03352,13.352,6.52914, m_useDebugMessages);
    HCr_LB  = new CombinedPT_calculator (0.97682,0.92168,0.0466287,0.0659354,0.623793,1.03018,20,20, m_useDebugMessages);
    HCr_HB  = new CombinedPT_calculator (0.964264,0.9,0.0509433,0.0955829,0.570585,1.331,20,20, m_useDebugMessages);
    HF_LB   = new CombinedPT_calculator (0.994936,0.914105,0.0231295,0.0885208,1.38838,3.27109,3.70499,6.48584, m_useDebugMessages);
    HF_HB   = new CombinedPT_calculator (0.98951,0.9,0.0369475,0.1,1.0458,3.09098,6.02728,3.84559, m_useDebugMessages);
  } //else if (m_mcPeriod == 1) 
  
  else if (m_mcPeriod == 2) {    
    std::cout << "CombinedPT_manager: Using MC12a PDFs " << std::endl;
    
    // PDF for set 12a
    //                                 (meanCluster, meanTrack, sigmaCluster, sigmaTrack, aCluster, aTrack,   nCluster,  nTrack, bool useDebug)
    LC_LB  = new CombinedPT_calculator(0.999857,1.00812,0.0285529,0.0238,1.31525,0.78,4.32744,20,m_useDebugMessages);
    LM_LB  = new CombinedPT_calculator(1.00054,1.00995,0.0396595,0.0329,0.831288,0.65,16.146,20,m_useDebugMessages);
    LCr_LB = new CombinedPT_calculator(1.01013,1.01217,0.0702888,0.0516118,1.04783,0.747204,20,20,m_useDebugMessages);
    LF_LB  = new CombinedPT_calculator(0.993938,0.994225,0.0385883,0.0767763,0.779654,1.17037,12.7391,20,m_useDebugMessages);
    MC_LB  = new CombinedPT_calculator(0.999064,1.00853,0.0213955,0.0234158,1.51857,0.94,3.88548,20,m_useDebugMessages);
    MM_LB  = new CombinedPT_calculator(0.999225,1.00884,0.0307302,0.0330593,0.999107,0.850306,15.5499,20,m_useDebugMessages);
    MCr_LB = new CombinedPT_calculator(1.00551,1.01171,0.0587745,0.0507085,1.17455,0.896958,20,19.9999,m_useDebugMessages);
    MF_LB  = new CombinedPT_calculator(0.995203,0.996467,0.0296621,0.0780902,1.02032,1.70235,8.64311,20,m_useDebugMessages);
    HC_LB  = new CombinedPT_calculator(0.9982,1.00665,0.0155868,0.0287011,1.91209,1.47552,3.367,20,m_useDebugMessages);
    HM_LB  = new CombinedPT_calculator(0.999373,1.00536,0.0213376,0.0411805,1.44767,1.58091,7.20785,20,m_useDebugMessages);
    HCr_LB = new CombinedPT_calculator(1.00064,1.00494,0.0482529,0.0588496,1.28708,1.31088,20,20,m_useDebugMessages);
    HF_LB  = new CombinedPT_calculator(0.997498,1.00121,0.021861,0.0850704,1.46484,2.56,5.21529,20,m_useDebugMessages);
    LC_HB  = new CombinedPT_calculator(0.991138,1.00075,0.0322101,0.0438198,0.731191,0.834,10.5051,20,m_useDebugMessages);
    LM_HB  = new CombinedPT_calculator(0.977691,0.997735,0.0481387,0.0536821,0.669644,0.670183,20,20,m_useDebugMessages);
    LCr_HB = new CombinedPT_calculator(0.971546,0.993738,0.0980446,0.0805773,1.383,0.875405,20,19.9999,m_useDebugMessages);
    LF_HB  = new CombinedPT_calculator(0.974772,0.980749,0.0483505,0.1,0.652043,0.807119,20,20,m_useDebugMessages);
    MC_HB  = new CombinedPT_calculator(0.99317,1.0017,0.0238569,0.0464191,1.03802,1.02,6.84362,20,m_useDebugMessages);
    MM_HB  = new CombinedPT_calculator(0.987494,0.999621,0.0351677,0.056589,0.816597,0.955575,20,20,m_useDebugMessages);
    MCr_HB = new CombinedPT_calculator(0.980456,0.997599,0.0740042,0.0850663,1.28946,1.03783,20,20,m_useDebugMessages);
    MF_HB  = new CombinedPT_calculator(0.98426,0.983793,0.0382004,0.1,0.904153,0.904153,11.5698,20,m_useDebugMessages);
    HC_HB  = new CombinedPT_calculator(0.995053,1.0004,0.0170857,0.06095,1.58075,1.3126,4.44681,20,m_useDebugMessages);
    HM_HB  = new CombinedPT_calculator(0.995205,1.00366,0.0236153,0.07088,1.14459,1.354,13.0934,20,m_useDebugMessages);
    HCr_HB = new CombinedPT_calculator(0.986288,0.997913,0.0553373,0.0991655,1.32606,1.42414,20,20,m_useDebugMessages);
    HF_HB  = new CombinedPT_calculator(0.994419,0.995653,0.027435,0.1,1.2292,0.820926,6.66118,20,m_useDebugMessages);
  } //else if (m_mcPeriod == 2) 

  else if (m_mcPeriod == 3) {

    std::cout << "CombinedPT_manager: Using MC12c PDFs " << std::endl;
   
    //PDF for set2 (GEO21+ MVA calibration)
    //                                 (meanCluster, meanTrack, sigmaCluster, sigmaTrack, aCluster, aTrack,   nCluster,  nTrack, bool useDebug)
    LC_LB  = new CombinedPT_calculator(0.999256,1.01096,0.0379299,0.0257768,1.23513,0.6072,6.03935,20,m_useDebugMessages);
    LC_HB  = new CombinedPT_calculator(0.990316,1.00415,0.0390609,0.0475633,0.636018,0.733716,20,20,m_useDebugMessages);
    LM_LB  = new CombinedPT_calculator(1.00542,1.01314,0.0569372,0.0353865,0.947486,0.471231,20,20,m_useDebugMessages);
    LM_HB  = new CombinedPT_calculator(0.962162,1.00705,0.0740513,0.0554795,1.06792,0.483221,20,20,m_useDebugMessages);
    LCr_LB = new CombinedPT_calculator(1.00457,1.00509,0.0998451,0.054087,1.50506,0.500542,20,20,m_useDebugMessages);
    LCr_HB = new CombinedPT_calculator(0.968354,1.00159,0.1,0.0782683,1.16295,0.499042,20,20,m_useDebugMessages);
    LF_LB  = new CombinedPT_calculator(0.996397,0.970643,0.0511971,0.1,0.92469,0.72785,16.1317,20,m_useDebugMessages);
    LF_HB  = new CombinedPT_calculator(0.974733,0.984092,0.0625375,0.1,0.801928,0.423595,20,20,m_useDebugMessages);
    MC_LB  = new CombinedPT_calculator(0.998788,1.01181,0.0239173,0.0278276,1.32477,0.578269,5.09617,20,m_useDebugMessages);
    MC_HB  = new CombinedPT_calculator(0.996472,1.00563,0.0244981,0.0508639,0.762607,0.645997,20,20,m_useDebugMessages);
    MM_LB  = new CombinedPT_calculator(1.00031,1.0121,0.0363936,0.0383205,1.00458,0.539382,20,20,m_useDebugMessages);
    MM_HB  = new CombinedPT_calculator(0.985524,1.00726,0.0432946,0.0598023,0.963309,0.546849,20,20,m_useDebugMessages);
    MCr_LB = new CombinedPT_calculator(0.993056,1.0087,0.0788928,0.0552659,1.42446,0.522046,20,20,m_useDebugMessages);
    MCr_HB = new CombinedPT_calculator(0.962507,0.99817,0.0861672,0.0869274,1.52628,0.549035,20,20,m_useDebugMessages);
    MF_LB  = new CombinedPT_calculator(0.995855,0.973331,0.0375446,0.1,1.19692,0.755036,6.97291,20,m_useDebugMessages);
    MF_HB  = new CombinedPT_calculator(0.987637,0.987221,0.0446646,0.1,0.920921,0.407055,19.1659,20,m_useDebugMessages);
    HC_LB  = new CombinedPT_calculator(0.999494,1.00856,0.012972,0.0468664,1.57776,0.743319,4.14421,20,m_useDebugMessages);
    HC_HB  = new CombinedPT_calculator(0.998461,0.991505,0.0143578,0.0787323,1.33713,0.61885,5.15624,20,m_useDebugMessages);
    HM_LB  = new CombinedPT_calculator(0.999807,1.00664,0.0174167,0.0597879,1.23301,0.594455,6.98754,20,m_useDebugMessages);
    HM_HB  = new CombinedPT_calculator(0.997562,1.00212,0.019206,0.0868666,1.00599,0.536711,11.0724,20,m_useDebugMessages);
    HCr_LB = new CombinedPT_calculator(0.984137,1.00281,0.0538298,0.0737061,1.23222,0.544143,20,20,m_useDebugMessages);
    HCr_HB = new CombinedPT_calculator(0.973345,1.00093,0.0565404,0.1,1.22125,0.454248,20,20,m_useDebugMessages);
    HF_LB  = new CombinedPT_calculator(0.998083,0.985457,0.0253153,0.1,1.55429,0.529694,4.13449,20,m_useDebugMessages);
    HF_HB= new CombinedPT_calculator(0.995999,1.00276,0.0317999,0.1,1.41971,0.268775,5.0814,20,m_useDebugMessages);
  } //  else if (m_mcPeriod == 3) 


  else if (m_mcPeriod == 2015) {

    std::cout << "CombinedPT_manager: Using MC15c PDFs " << std::endl; // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/EgammaCalibration#MC15c_20_7_5_single_particle_sam
   
    //PDF for MC15c (20.7.5) single particle samples without pileup https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/EgammaCalibration#MC15c_20_7_5_single_particle_sam
//                                      meanCalo,   meanTrack,  sigmaCalo,  sigmaTrack, aCalo,      aTrack,     nCalo,      nTrack,     meanGCalo,  meanGTrack, sigmaGCalo, sigmaGTrack,    caloCBWeight,   trackCBWeight,
LC_LB   = new CombinedPT_calculator(    0.957439,   0.962388,   0.075966,   0.046045,   1.209373,   0.869374,   8.815730,   0.608145,   1.000318,   1.002211,   0.035527,   0.018084,   0.257453,   0.593711,   m_useDebugMessages);
LC_HB   = new CombinedPT_calculator(    0.991291,   0.999544,   0.038018,   0.032561,   0.718346,   1.050914,   10.516080,  0.635189,   0.900000,   1.046561,   0.096818,   0.132521,   0.760358,   0.928978,   m_useDebugMessages);
LM_LB   = new CombinedPT_calculator(    1.008058,   0.996002,   0.056622,   0.030888,   0.875944,   0.897065,   19.999988,  0.669802,   0.985839,   0.986674,   0.106883,   0.075944,   0.822293,   0.942096,   m_useDebugMessages);
LM_HB   = new CombinedPT_calculator(    0.985658,   0.996368,   0.058687,   0.039986,   0.703842,   0.805899,   19.999996,  0.554022,   0.919873,   1.049101,   0.105583,   0.126731,   0.514646,   0.917535,   m_useDebugMessages);
LCr_LB  = new CombinedPT_calculator(    1.027303,   0.992161,   0.087405,   0.043995,   0.959805,   0.582601,   19.999795,  0.689711,   0.979467,   1.002635,   0.156263,   0.111808,   0.622541,   0.973354,   m_useDebugMessages);
LCr_HB  = new CombinedPT_calculator(    0.900593,   0.900000,   0.150779,   0.146164,   -4.750847,  0.522222,   14.486653,  0.290216,   0.986396,   0.997658,   0.087675,   0.045889,   0.700185,   0.792972,   m_useDebugMessages);
LF_LB   = new CombinedPT_calculator(    1.005425,   0.900012,   0.031599,   0.128081,   0.729253,   0.170446,   6.711304,   0.828750,   0.984586,   1.001710,   0.121704,   0.053865,   0.740721,   0.829666,   m_useDebugMessages);
LF_HB   = new CombinedPT_calculator(    0.993849,   0.968371,   0.035055,   0.090336,   0.429187,   0.107857,   20.000000,  0.038148,   0.948603,   1.082689,   0.127201,   0.171381,   0.701747,   0.737177,   m_useDebugMessages);
MC_LB   = new CombinedPT_calculator(    0.961670,   0.960863,   0.054639,   0.046338,   1.744215,   0.873892,   2.500954,   0.617002,   0.999910,   1.002796,   0.022509,   0.019493,   0.190165,   0.619633,   m_useDebugMessages);
MC_HB   = new CombinedPT_calculator(    0.952190,   1.001215,   0.052593,   0.039006,   1.594593,   0.983870,   6.372608,   0.613231,   0.996459,   1.043417,   0.022782,   0.126664,   0.359434,   0.938423,   m_useDebugMessages);
MM_LB   = new CombinedPT_calculator(    1.003071,   0.932158,   0.035731,   0.072347,   0.894313,   0.721916,   19.999999,  0.430112,   0.997890,   0.999679,   0.080920,   0.029746,   0.945907,   0.635636,   m_useDebugMessages);
MM_HB   = new CombinedPT_calculator(    0.992856,   0.997058,   0.037057,   0.044791,   0.803172,   0.811691,   20.000000,  0.491081,   0.958506,   1.037030,   0.070938,   0.121884,   0.727480,   0.920020,   m_useDebugMessages);
MCr_LB  = new CombinedPT_calculator(    1.008359,   0.991933,   0.058835,   0.044198,   1.058595,   0.617136,   19.999987,  0.536583,   1.000844,   0.970319,   0.110534,   0.100504,   0.704051,   0.930911,   m_useDebugMessages);
MCr_HB  = new CombinedPT_calculator(    0.986736,   0.995807,   0.059885,   0.059940,   0.907507,   0.422220,   19.109761,  0.378667,   0.964139,   1.059565,   0.110301,   0.139968,   0.620683,   0.882166,   m_useDebugMessages);
MF_LB   = new CombinedPT_calculator(    0.969932,   0.948926,   0.071469,   0.110043,   1.924988,   0.000283,   4.796465,   3.032847,   1.000129,   0.972230,   0.022693,   0.075939,   0.405166,   0.773936,   m_useDebugMessages);
MF_HB   = new CombinedPT_calculator(    0.996602,   0.970017,   0.026061,   0.093559,   0.551184,   0.147650,   13.756530,  0.000000,   0.986589,   1.111059,   0.100504,   0.163928,   0.852487,   0.787314,   m_useDebugMessages);
HC_LB   = new CombinedPT_calculator(    1.000040,   0.998662,   0.010184,   0.039236,   1.472715,   0.620740,   3.121541,   1.673746,   1.012286,   1.017210,   0.070385,   0.090287,   0.985696,   0.888707,   m_useDebugMessages);
HC_HB   = new CombinedPT_calculator(    0.999562,   0.995555,   0.009332,   0.063372,   1.246537,   0.452725,   3.519579,   1.298845,   1.007699,   1.024103,   0.073310,   0.164118,   0.983178,   0.737382,   m_useDebugMessages);
HM_LB   = new CombinedPT_calculator(    1.000318,   0.991727,   0.012997,   0.052339,   1.220167,   0.522588,   4.183401,   1.641324,   1.006229,   1.021027,   0.053372,   0.110229,   0.950892,   0.861635,   m_useDebugMessages);
HM_HB   = new CombinedPT_calculator(    0.999211,   0.993598,   0.012085,   0.071905,   1.063829,   0.428594,   4.393848,   1.355637,   1.003338,   1.057601,   0.055862,   0.149316,   0.947893,   0.761789,   m_useDebugMessages);
HCr_LB  = new CombinedPT_calculator(    0.987992,   0.989248,   0.068807,   0.061635,   1.663581,   0.346296,   6.504665,   2.116906,   1.001547,   1.033103,   0.027315,   0.115374,   0.243699,   0.892271,   m_useDebugMessages);
HCr_HB  = new CombinedPT_calculator(    0.974860,   0.964621,   0.073209,   0.200000,   1.464359,   0.000251,   11.390826,  19.990490,  0.998319,   0.978911,   0.027423,   0.120080,   0.229431,   0.545600,   m_useDebugMessages);
HF_LB   = new CombinedPT_calculator(    0.999984,   0.982950,   0.011460,   0.098553,   1.037470,   0.210918,   4.516410,   3.247951,   1.008146,   1.200000,   0.069964,   0.105814,   0.934694,   0.944647,   m_useDebugMessages);
HF_HB   = new CombinedPT_calculator(    0.999439,   0.955036,   0.011114,   0.149262,   1.013976,   0.170739,   4.171553,   20.000000,  1.003032,   1.200000,   0.069345,   0.200000,   0.912189,   0.811742,   m_useDebugMessages);


  } //  else if (m_mcPeriod == 2015) 

  else if (m_mcPeriod == 2016) {

    std::cout << "CombinedPT_manager: Using MC16 PDFs " << std::endl; // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/EgammaCalibration#MC15c_20_7_5_single_particle_sam
   
    //PDF for MC16a (21.?.?) 
//                                      meanCalo,   meanTrack,  sigmaCalo,  sigmaTrack, aCalo,      aTrack,     nCalo,      nTrack,     meanGCalo,  meanGTrack, sigmaGCalo, sigmaGTrack,    caloCBWeight,   trackCBWeight,
vLC_LB  = new CombinedPT_calculator(    0.995455,   0.952527,   0.055876,   0.060958,   1.515472,   0.863814,   1.907991,   0.607251,   1.041055,   1.001090,   0.108486,   0.019208,   0.780655,   0.542723,   m_useDebugMessages);
vLC_HB  = new CombinedPT_calculator(    0.978001,   0.936474,   0.066259,   0.084948,   0.968797,   0.807880,   15.083635,  0.449395,   1.146902,   1.000767,   0.044159,   0.025658,   0.985445,   0.609527,   m_useDebugMessages);
vLM_LB  = new CombinedPT_calculator(    1.001965,   0.946940,   0.063707,   0.074972,   0.366994,   0.704700,   19.999956,  0.632219,   1.028275,   1.000708,   0.106887,   0.026661,   0.237288,   0.654154,   m_useDebugMessages);
vLM_HB  = new CombinedPT_calculator(    0.936383,   0.996831,   0.102160,   0.035889,   1.238764,   0.777353,   2.531818,   0.486183,   0.991056,   1.013853,   0.116642,   0.085453,   0.473107,   0.879543,   m_useDebugMessages);
vLCr_LB = new CombinedPT_calculator(    1.196741,   0.998133,   0.002326,   0.035910,   0.003398,   0.619056,   19.995921,  0.477022,   1.000925,   1.006229,   0.135683,   0.081583,   0.180055,   0.872940,   m_useDebugMessages);
vLCr_HB = new CombinedPT_calculator(    0.901228,   0.986108,   0.074358,   0.056682,   1.281002,   0.905537,   0.000003,   0.115097,   0.950314,   0.975442,   0.145684,   0.182205,   0.146070,   0.867907,   m_useDebugMessages);
vLF_LB  = new CombinedPT_calculator(    1.002265,   0.900000,   0.044388,   0.129936,   0.742066,   0.363539,   3.563237,   0.379937,   1.083777,   1.004680,   0.115394,   0.050298,   0.847077,   0.841762,   m_useDebugMessages);
vLF_HB  = new CombinedPT_calculator(    0.987636,   0.927477,   0.050042,   0.149490,   0.567183,   0.326471,   6.058409,   0.201656,   1.027383,   1.013626,   0.122592,   0.045847,   0.763954,   0.966842,   m_useDebugMessages);
LC_LB   = new CombinedPT_calculator(    0.973466,   0.960461,   0.077383,   0.048071,   1.832549,   0.871540,   1.450166,   0.577852,   1.001819,   1.001923,   0.034778,   0.018702,   0.249330,   0.598952,   m_useDebugMessages);
LC_HB   = new CombinedPT_calculator(    0.994247,   0.946900,   0.037341,   0.078669,   1.271691,   0.760610,   3.010080,   0.514091,   0.971237,   1.003066,   0.071640,   0.027269,   0.771544,   0.641088,   m_useDebugMessages);
LM_LB   = new CombinedPT_calculator(    1.007450,   0.947515,   0.047762,   0.064959,   1.231214,   0.723798,   2.381220,   0.504453,   0.998452,   1.000985,   0.079933,   0.026491,   0.500405,   0.677101,   m_useDebugMessages);
LM_HB   = new CombinedPT_calculator(    0.961383,   0.930267,   0.085229,   0.090809,   1.750306,   0.653096,   2.553173,   0.362892,   1.000734,   1.001857,   0.047926,   0.032843,   0.730651,   0.721707,   m_useDebugMessages);
LCr_LB  = new CombinedPT_calculator(    1.043306,   0.990482,   0.110188,   0.044512,   1.024082,   0.586232,   19.999983,  0.629820,   0.988267,   0.900099,   0.092923,   0.200000,   0.641911,   0.965289,   m_useDebugMessages);
LCr_HB  = new CombinedPT_calculator(    0.970657,   0.900000,   0.101493,   0.122128,   1.156101,   0.438444,   19.787640,  0.237509,   1.033766,   1.002094,   0.147152,   0.044280,   0.725241,   0.821499,   m_useDebugMessages);
LF_LB   = new CombinedPT_calculator(    1.002292,   0.900000,   0.030419,   0.123890,   0.736521,   0.466854,   4.082955,   0.209875,   1.048625,   1.007902,   0.081735,   0.051577,   0.839671,   0.858212,   m_useDebugMessages);
LF_HB   = new CombinedPT_calculator(    0.995238,   0.966281,   0.033931,   0.122592,   0.603633,   0.325761,   5.705330,   0.148764,   1.025918,   1.152345,   0.089577,   0.195961,   0.791804,   1.000000,   m_useDebugMessages);
MC_LB   = new CombinedPT_calculator(    0.973812,   0.961763,   0.049563,   0.045733,   1.680767,   0.852445,   2.147141,   0.638757,   0.999824,   1.003347,   0.022177,   0.019580,   0.173504,   0.647431,   m_useDebugMessages);
MC_HB   = new CombinedPT_calculator(    0.967362,   0.955339,   0.047807,   0.081445,   1.767002,   0.749202,   2.240168,   0.486649,   0.997581,   1.007829,   0.022708,   0.033021,   0.246331,   0.692478,   m_useDebugMessages);
MM_LB   = new CombinedPT_calculator(    0.976891,   0.944430,   0.057532,   0.065805,   1.826973,   0.722690,   2.156610,   0.474415,   1.003097,   1.001639,   0.030493,   0.028555,   0.348103,   0.689549,   m_useDebugMessages);
MM_HB   = new CombinedPT_calculator(    0.996384,   0.943428,   0.031980,   0.088043,   1.136103,   0.628350,   3.443792,   0.382633,   0.979094,   1.006563,   0.053338,   0.035870,   0.619352,   0.755853,   m_useDebugMessages);
MCr_LB  = new CombinedPT_calculator(    1.001028,   0.993369,   0.052716,   0.041109,   0.968441,   0.465255,   9.192577,   0.544478,   1.033245,   0.985252,   0.095170,   0.075368,   0.543053,   0.889018,   m_useDebugMessages);
MCr_HB  = new CombinedPT_calculator(    0.988231,   0.908293,   0.072373,   0.120222,   1.240556,   0.397696,   19.999998,  0.264065,   1.171513,   1.010127,   0.049008,   0.050267,   0.968163,   0.813616,   m_useDebugMessages);
MF_LB   = new CombinedPT_calculator(    0.976516,   0.900000,   0.065011,   0.124053,   1.538115,   0.379079,   3.846562,   0.170541,   0.998827,   1.011980,   0.020975,   0.052217,   0.356515,   0.866721,   m_useDebugMessages);
MF_HB   = new CombinedPT_calculator(    0.971990,   0.974293,   0.066726,   0.125183,   1.495121,   0.333325,   4.167515,   0.057890,   0.995587,   1.125844,   0.022848,   0.199991,   0.431686,   1.000000,   m_useDebugMessages);
HC_LB   = new CombinedPT_calculator(    0.999956,   0.957555,   0.009771,   0.082347,   1.598061,   0.764285,   2.960983,   1.789786,   1.008497,   1.004007,   0.045335,   0.032093,   0.975091,   0.726836,   m_useDebugMessages);
HC_HB   = new CombinedPT_calculator(    0.999654,   0.971719,   0.009076,   0.112547,   1.467668,   0.567053,   3.098246,   2.927637,   1.007833,   1.014620,   0.042205,   0.038599,   0.971439,   0.927941,   m_useDebugMessages);
HM_LB   = new CombinedPT_calculator(    0.992745,   0.948220,   0.028270,   0.100815,   1.680345,   0.709997,   3.140151,   1.692642,   1.000295,   1.001947,   0.010652,   0.040319,   0.258188,   0.799175,   m_useDebugMessages);
HM_HB   = new CombinedPT_calculator(    0.990867,   0.988128,   0.027541,   0.071765,   1.696795,   0.214829,   2.961753,   17.910572,  0.999451,   1.041677,   0.010022,   0.108073,   0.287547,   0.721288,   m_useDebugMessages);
HCr_LB  = new CombinedPT_calculator(    0.995143,   0.954314,   0.064319,   0.094512,   2.047072,   0.482755,   9.502915,   2.117815,   1.001586,   1.013441,   0.026029,   0.040676,   0.515777,   0.903444,   m_useDebugMessages);
HCr_HB  = new CombinedPT_calculator(    0.998967,   0.900000,   0.028295,   0.157486,   0.803407,   0.419227,   19.814205,  5.382640,   1.017054,   1.031887,   0.060562,   0.079755,   0.718215,   0.766918,   m_useDebugMessages);
HF_LB   = new CombinedPT_calculator(    1.000022,   0.972059,   0.009727,   0.123001,   1.255295,   0.237377,   2.840160,   3.377217,   1.006833,   1.024951,   0.045462,   0.027001,   0.906236,   0.993377,   m_useDebugMessages);
HF_HB   = new CombinedPT_calculator(    0.975327,   1.019038,   0.067701,   0.143776,   7.783802,   0.183061,   0.248577,   0.995200,   0.998483,   0.900035,   0.011258,   0.151595,   0.159316,   0.916354,   m_useDebugMessages);



  } //  else if (m_mcPeriod == 2015)
} // CombinedPT_manager::CombinedPT_manager(int mcPeriod, bool useDebug)
  

  CombinedPT_manager::~CombinedPT_manager()
{

  if (m_useDebugMessages) std::cout << "Destructing CombinedPT_manager!"<< std::endl;

  delete LC_LB;
  delete LM_LB;
  delete LCr_LB;
  delete LF_LB;
  delete MC_LB;
  delete MM_LB;
  delete MCr_LB;
  delete MF_LB;
  delete HC_LB;
  delete HM_LB;
  delete HCr_LB;
  delete HF_LB;
  delete LC_HB;
  delete LM_HB;
  delete LCr_HB;
  delete LF_HB;
  delete MC_HB;
  delete MM_HB;
  delete MCr_HB;
  delete MF_HB;
  delete HC_HB;
  delete HM_HB;
  delete HCr_HB;
  delete HF_HB;
} //   CombinedPT_manager::~CombinedPT_manager()


void CombinedPT_manager::getCombinedPt (double pT_track,double pT_cluster, double eta_track, double fBrem, double &ptCombined, double &ptError)
{


  double veryLowPtLimit_5GeV = 5.;
  double lowPtLimit_7GeV = 7.;
  double mediumPtLimit_15GeV = 15.;
  double highPtLimit_30GeV = 30.;
  
  //Rescale pT region limits if GeV not used.
  if( !m_useGeV ) {
    veryLowPtLimit_5GeV *= 1000;
    lowPtLimit_7GeV     *= 1000;
    mediumPtLimit_15GeV *= 1000;
    highPtLimit_30GeV   *= 1000;
  }
    
  ptCombined=-999.;
  ptError=-999.;
  
  //Check for proper brem value.
  fBrem = fabs(fBrem);
  if (fBrem>1 )
    {
      if (m_useDebugMessages) std::cout << "WARNING: Require fBrem in [0,1]-> ptCluster and clusterResolution returned" << std::endl;
      ptCombined=pT_cluster;
      ptError=-999;
      return;
    } // if(fBrem>1 )



 if(fabs(eta_track)>2.5)
    {
      if (m_useDebugMessages) std::cout<<"WARNING: Electron etaTrack outside of combination range, returning cluster energy and error"<< std::endl;
      ptCombined=pT_cluster;
      ptError=-999;
      return;
    }// if(fabs(eta_track)>2.5)

  //Checks for sufficient Pt and fiducial eta.
  if(pT_cluster<veryLowPtLimit_5GeV)
    {
      if (m_useDebugMessages) std::cout<<"WARNING: Electron pt outside of combination range-> ptCluster and clusterResolution returned"<< std::endl;
      ptCombined=pT_cluster;
      ptError=-999;
      return;
    }// if(pt<lowPtLimit_7GeV)

/////////////////////////////////////////////////////////////////////////////////////////

  // now the variables should be good. Funnel my pT ratios in the right histograms
  // we structure the funneling in this way: check for pt, then for eta, then for fBrem
    
    if(pT_cluster<=lowPtLimit_7GeV) // very Low pT
      {   if(fabs(eta_track)<=0.7) // Central Eta
          {   
            if(fBrem <= 0.2){ vLC_LB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // Low Brems
            else{             vLC_HB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // High Brems
          }             
          else if(fabs(eta_track)<=1.37) // Medium Eta
          {
            if(fBrem <= 0.3){ vLM_LB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // Low Brems
            else{             vLM_HB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // High Brems
          }
          else if(fabs(eta_track)<=1.52) // Creck Eta
          {         
            if(fBrem <= 0.3){ vLCr_LB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // Low Brems
            else{             vLCr_HB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // High Brems
          }
          else if(fabs(eta_track)<=2.5) // Forward eta
          {
            if(fBrem <= 0.3){ vLF_LB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // Low Brems
            else{             vLF_HB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // High Brems
          }
            
      }//if(pt<=mediumPtLimit_15GeV)


      else  if(pT_cluster<=mediumPtLimit_15GeV) // Low pT
      {   if(fabs(eta_track)<=0.7) // Central Eta
          {   
            if(fBrem <= 0.2){ LC_LB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // Low Brems
            else{             LC_HB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // High Brems
          }             
          else if(fabs(eta_track)<=1.37) // Medium Eta
          {
            if(fBrem <= 0.3){ LM_LB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // Low Brems
            else{             LM_HB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // High Brems
          }
          else if(fabs(eta_track)<=1.52) // Creck Eta
          {         
            if(fBrem <= 0.3){ LCr_LB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // Low Brems
            else{             LCr_HB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // High Brems
          }
          else if(fabs(eta_track)<=2.5) // Forward eta
          {
            if(fBrem <= 0.3){ LF_LB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // Low Brems
            else{             LF_HB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // High Brems
          }
            
      }//if(pt<=mediumPtLimit_15GeV)


      else  if(pT_cluster<=highPtLimit_30GeV) // medium pT
      {   if(fabs(eta_track)<=0.7) // Central Eta
          {   
            if(fBrem <= 0.2){ MC_LB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // Low Brems
            else{             MC_HB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // High Brems
          }
          else if(fabs(eta_track)<=1.37) // Medium Eta
          {
            if(fBrem <= 0.3){ MM_LB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // Low Brems
            else{             MM_HB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // High Brems
          }
          else if(fabs(eta_track)<=1.52) // Creck Eta
          {         
            if(fBrem <= 0.3){ MCr_LB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // Low Brems
            else{             MCr_HB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // High Brems
          }
          else if(fabs(eta_track)<=2.5) // Forward eta
          {
            if(fBrem <= 0.3){ MF_LB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // Low Brems
            else{             MF_HB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // High Brems
          }
            
      }//if(pt<=highPtLimit_30GeV)
      else  // high pT
      {   if(fabs(eta_track)<=0.7) // Central Eta
          {   
            if(fBrem <= 0.2){ HC_LB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // Low Brems
            else{             HC_HB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // High Brems
          }
          else if(fabs(eta_track)<=1.37) // Medium Eta
          {
            if(fBrem <= 0.3){ HM_LB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // Low Brems
            else{             HM_HB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // High Brems
          }
          else if(fabs(eta_track)<=1.52) // Creck Eta
          {         
            if(fBrem <= 0.3){ HCr_LB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // Low Brems
            else{             HCr_HB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // High Brems
          }
          else if(fabs(eta_track)<=2.5) // Forward eta
          {
            if(fBrem <= 0.3){ HF_LB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // Low Brems
            else{             HF_HB->Evaluate(pT_track,pT_cluster,ptCombined,ptError);return; } // High Brems
          }
            
      }// else // high pT

} // void CombinedPT_manager::getCombinedPt

#endif

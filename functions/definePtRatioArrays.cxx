#include "definePtRatioArrays.h"
#include "TH1D.h"

void defineArrays(TH1D* TrackPT_condCut[32], // Tracker pT, fill this one if the event passes the Combination cut, so that we can use it for fitting the PDF
                  TH1D* CaloPT_condCut[32],  // Calo pT, fill this one if the event passes the Combination cut, so that we can use it for fitting the PDF
                  TH1D* CombPT_condCut[32], // Combined pT, fill this one if we do the combination
                  TH1D* TrackPT_condComb[32], // Tracker pT, fill this one if we do the combination, but do the classification according to the combined pT, i.e. if the tracker pT is a bajillion, but the combined is zero, put the event in the low Pt bin in this array
                  TH1D* CaloPT_condComb[32], // Calo pT, fill this one if we do the combination, but do the classification according to the combined pT, i.e. if the calo pT is a bajillion, but the combined is zero, put the event in the low Pt bin in this array
                  TH1D* CaloOrCombPT_condNone[32], // Calo or Combined pT, put fill with Calo pT, unless we do the combination, then fill with the combined pT
                  TH1D* Calo_condNone[32], // Calo pT, just the pT of the Calorimeter, nothe else, no cuts, no conditions
                  TH1D* Track_condNone[32], // Track pT, just the pT of the Tracker, nothe else, no cuts, no conditions
                  TH1D* Calo_condCaloOrComb[32],
                  TH1D* Track_condCaloOrComb[32])
{  
   int nBins = 1500;
   double lowerLimit = 0.;
   double upperLimit = 3.;

   // Tracker

   // initialize the arrays to store in the electron momentum eTrackerPT/eTruePt radios
   // The names of the histrograms are constructed as 
   // [ptTrack LOW,MEDIUM,HIGH][eta CENTRAL,MEDIUM, CRACK, FORWARD][fBrem HIGHBREM,LOWBREM]
   // i.e. LC_LB means Low PT, Central eta, _ , LowBrems
   TrackPT_condCut[ 0] = new TH1D("Trk, vL_C_LB"  , "Very Low pT, Central eta, Low Brems - 1/32"     , nBins, lowerLimit, upperLimit);
   TrackPT_condCut[ 1] = new TH1D("Trk, vL_C_HB"  , "Very Low pT, Central eta, High Brems - 2/32"    , nBins, lowerLimit, upperLimit);
   TrackPT_condCut[ 2] = new TH1D("Trk, vL_M_LB"  , "Very Low pT, Medium eta, Low Brems - 3/32"      , nBins, lowerLimit, upperLimit);
   TrackPT_condCut[ 3] = new TH1D("Trk, vL_M_HB"  , "Very Low pT, Medium eta, High Brems - 4/32"     , nBins, lowerLimit, upperLimit);
   TrackPT_condCut[ 4] = new TH1D("Trk, vL_Cr_LB" , "Very Low pT, Crack eta, Low Brems - 5/32"       , nBins, lowerLimit, upperLimit);
   TrackPT_condCut[ 5] = new TH1D("Trk, vL_Cr_HB" , "Very Low pT, Crack eta, High Brems - 6/32"      , nBins, lowerLimit, upperLimit);
   TrackPT_condCut[ 6] = new TH1D("Trk, vL_F_LB"  , "Very Low pT, Forward eta, Low Brems - 7/32"     , nBins, lowerLimit, upperLimit);
   TrackPT_condCut[ 7] = new TH1D("Trk, vL_F_HB"  , "Very Low pT, Forward eta, High Brems - 8/32"    , nBins, lowerLimit, upperLimit);

   TrackPT_condCut[ 8] = new TH1D("Trk, L_C_LB"   , "Low pT, Central eta, Low Brems - 9/32"  , nBins, lowerLimit, upperLimit);
   TrackPT_condCut[ 9] = new TH1D("Trk, L_C_HB"   , "Low pT, Central eta, High Brems - 10/32", nBins, lowerLimit, upperLimit);
   TrackPT_condCut[10] = new TH1D("Trk, L_M_LB"   , "Low pT, Medium eta, Low Brems - 11/32"  , nBins, lowerLimit, upperLimit);
   TrackPT_condCut[11] = new TH1D("Trk, L_M_HB"   , "Low pT, Medium eta, High Brems - 12/32" , nBins, lowerLimit, upperLimit);
   TrackPT_condCut[12] = new TH1D("Trk, L_Cr_LB"  , "Low pT, Crack eta, Low Brems - 13/32"   , nBins, lowerLimit, upperLimit);
   TrackPT_condCut[13] = new TH1D("Trk, L_Cr_HB"  , "Low pT, Crack eta, High Brems - 14/32"  , nBins, lowerLimit, upperLimit);
   TrackPT_condCut[14] = new TH1D("Trk, L_F_LB"   , "Low pT, Forward eta, Low Brems - 15/32" , nBins, lowerLimit, upperLimit);
   TrackPT_condCut[15] = new TH1D("Trk, L_F_HB"   , "Low pT, Forward eta, High Brems - 16/32", nBins, lowerLimit, upperLimit);

   TrackPT_condCut[16] = new TH1D("Trk, M_C_LB"   , "Medium pT Central eta, Low Brems - 17/32"   , nBins, lowerLimit, upperLimit);
   TrackPT_condCut[17] = new TH1D("Trk, M_C_HB"   , "Medium pT Central eta, High Brems - 18/32"  , nBins, lowerLimit, upperLimit);
   TrackPT_condCut[18] = new TH1D("Trk, M_M_LB"   , "Medium pT Medium eta, Low Brems - 19/32"    , nBins, lowerLimit, upperLimit);
   TrackPT_condCut[19] = new TH1D("Trk, M_M_HB"   , "Medium pT Medium eta, High Brems - 20/32"   , nBins, lowerLimit, upperLimit);
   TrackPT_condCut[20] = new TH1D("Trk, M_Cr_LB"  , "Medium pT Crack eta, Low Brems - 21/32"     , nBins, lowerLimit, upperLimit);
   TrackPT_condCut[21] = new TH1D("Trk, M_Cr_HB"  , "Medium pT Crack eta, High Brems - 22/32"    , nBins, lowerLimit, upperLimit);
   TrackPT_condCut[22] = new TH1D("Trk, M_F_LB"   , "Medium pT Forward eta, Low Brems - 23/32"   , nBins, lowerLimit, upperLimit);
   TrackPT_condCut[23] = new TH1D("Trk, M_F_HB"   , "Medium pT Forward eta, High Brems - 24/32"  , nBins, lowerLimit, upperLimit);

   TrackPT_condCut[24] = new TH1D("Trk, H_C_LB"   , "High pT, Central eta, Low Brems - 25/32"   , nBins, lowerLimit, upperLimit);
   TrackPT_condCut[25] = new TH1D("Trk, H_C_HB"   , "High pT, Central eta, High Brems - 26/32"  , nBins, lowerLimit, upperLimit);
   TrackPT_condCut[26] = new TH1D("Trk, H_M_LB"   , "High pT, Medium eta, Low Brems - 27/32"    , nBins, lowerLimit, upperLimit);
   TrackPT_condCut[27] = new TH1D("Trk, H_M_HB"   , "High pT, Medium eta, High Brems - 28/32"   , nBins, lowerLimit, upperLimit);
   TrackPT_condCut[28] = new TH1D("Trk, H_Cr_LB"  , "High pT, Crack eta, Low Brems - 29/32"     , nBins, lowerLimit, upperLimit);
   TrackPT_condCut[29] = new TH1D("Trk, H_Cr_HB"  , "High pT, Crack eta, High Brems - 30/32"    , nBins, lowerLimit, upperLimit);
   TrackPT_condCut[30] = new TH1D("Trk, H_F_LB"   , "High pT, Forward eta, Low Brems - 31/32"   , nBins, lowerLimit, upperLimit);
   TrackPT_condCut[31] = new TH1D("Trk, H_F_HB"   , "High pT, Forward eta, High Brems - 32/32"  , nBins, lowerLimit, upperLimit);

   // Calorimeter
   CaloPT_condCut[ 0] = new TH1D("Calo, vL_C_LB"  , "Very Low pT, Central eta, Low Brems - 1/32"     , nBins, lowerLimit, upperLimit);
   CaloPT_condCut[ 1] = new TH1D("Calo, vL_C_HB"  , "Very Low pT, Central eta, High Brems - 2/32"    , nBins, lowerLimit, upperLimit);
   CaloPT_condCut[ 2] = new TH1D("Calo, vL_M_LB"  , "Very Low pT, Medium eta, Low Brems - 3/32"      , nBins, lowerLimit, upperLimit);
   CaloPT_condCut[ 3] = new TH1D("Calo, vL_M_HB"  , "Very Low pT, Medium eta, High Brems - 4/32"     , nBins, lowerLimit, upperLimit);
   CaloPT_condCut[ 4] = new TH1D("Calo, vL_Cr_LB" , "Very Low pT, Crack eta, Low Brems - 5/32"       , nBins, lowerLimit, upperLimit);
   CaloPT_condCut[ 5] = new TH1D("Calo, vL_Cr_HB" , "Very Low pT, Crack eta, High Brems - 6/32"      , nBins, lowerLimit, upperLimit);
   CaloPT_condCut[ 6] = new TH1D("Calo, vL_F_LB"  , "Very Low pT, Forward eta, Low Brems - 7/32"     , nBins, lowerLimit, upperLimit);
   CaloPT_condCut[ 7] = new TH1D("Calo, vL_F_HB"  , "Very Low pT, Forward eta, High Brems - 8/32"    , nBins, lowerLimit, upperLimit);

   CaloPT_condCut[ 8] = new TH1D("Calo, L_C_LB"   , "Low pT, Central eta, Low Brems - 9/32"  , nBins, lowerLimit, upperLimit);
   CaloPT_condCut[ 9] = new TH1D("Calo, L_C_HB"   , "Low pT, Central eta, High Brems - 10/32", nBins, lowerLimit, upperLimit);
   CaloPT_condCut[10] = new TH1D("Calo, L_M_LB"   , "Low pT, Medium eta, Low Brems - 11/32"  , nBins, lowerLimit, upperLimit);
   CaloPT_condCut[11] = new TH1D("Calo, L_M_HB"   , "Low pT, Medium eta, High Brems - 12/32" , nBins, lowerLimit, upperLimit);
   CaloPT_condCut[12] = new TH1D("Calo, L_Cr_LB"  , "Low pT, Crack eta, Low Brems - 13/32"   , nBins, lowerLimit, upperLimit);
   CaloPT_condCut[13] = new TH1D("Calo, L_Cr_HB"  , "Low pT, Crack eta, High Brems - 14/32"  , nBins, lowerLimit, upperLimit);
   CaloPT_condCut[14] = new TH1D("Calo, L_F_LB"   , "Low pT, Forward eta, Low Brems - 15/32" , nBins, lowerLimit, upperLimit);
   CaloPT_condCut[15] = new TH1D("Calo, L_F_HB"   , "Low pT, Forward eta, High Brems - 16/32", nBins, lowerLimit, upperLimit);

   CaloPT_condCut[16] = new TH1D("Calo, M_C_LB"   , "Medium pT Central eta, Low Brems - 17/32"   , nBins, lowerLimit, upperLimit);
   CaloPT_condCut[17] = new TH1D("Calo, M_C_HB"   , "Medium pT Central eta, High Brems - 18/32"  , nBins, lowerLimit, upperLimit);
   CaloPT_condCut[18] = new TH1D("Calo, M_M_LB"   , "Medium pT Medium eta, Low Brems - 19/32"    , nBins, lowerLimit, upperLimit);
   CaloPT_condCut[19] = new TH1D("Calo, M_M_HB"   , "Medium pT Medium eta, High Brems - 20/32"   , nBins, lowerLimit, upperLimit);
   CaloPT_condCut[20] = new TH1D("Calo, M_Cr_LB"  , "Medium pT Crack eta, Low Brems - 21/32"     , nBins, lowerLimit, upperLimit);
   CaloPT_condCut[21] = new TH1D("Calo, M_Cr_HB"  , "Medium pT Crack eta, High Brems - 22/32"    , nBins, lowerLimit, upperLimit);
   CaloPT_condCut[22] = new TH1D("Calo, M_F_LB"   , "Medium pT Forward eta, Low Brems - 23/32"   , nBins, lowerLimit, upperLimit);
   CaloPT_condCut[23] = new TH1D("Calo, M_F_HB"   , "Medium pT Forward eta, High Brems - 24/32"  , nBins, lowerLimit, upperLimit);

   CaloPT_condCut[24] = new TH1D("Calo, H_C_LB"   , "High pT, Central eta, Low Brems - 25/32"   , nBins, lowerLimit, upperLimit);
   CaloPT_condCut[25] = new TH1D("Calo, H_C_HB"   , "High pT, Central eta, High Brems - 26/32"  , nBins, lowerLimit, upperLimit);
   CaloPT_condCut[26] = new TH1D("Calo, H_M_LB"   , "High pT, Medium eta, Low Brems - 27/32"    , nBins, lowerLimit, upperLimit);
   CaloPT_condCut[27] = new TH1D("Calo, H_M_HB"   , "High pT, Medium eta, High Brems - 28/32"   , nBins, lowerLimit, upperLimit);
   CaloPT_condCut[28] = new TH1D("Calo, H_Cr_LB"  , "High pT, Crack eta, Low Brems - 29/32"     , nBins, lowerLimit, upperLimit);
   CaloPT_condCut[29] = new TH1D("Calo, H_Cr_HB"  , "High pT, Crack eta, High Brems - 30/32"    , nBins, lowerLimit, upperLimit);
   CaloPT_condCut[30] = new TH1D("Calo, H_F_LB"   , "High pT, Forward eta, Low Brems - 31/32"   , nBins, lowerLimit, upperLimit);
   CaloPT_condCut[31] = new TH1D("Calo, H_F_HB"   , "High pT, Forward eta, High Brems - 32/32"  , nBins, lowerLimit, upperLimit);


   // Combined
   CombPT_condCut[ 0] = new TH1D("Comb, vL_C_LB"  , "Very Low pT, Central eta, Low Brems - 1/32"     , nBins, lowerLimit, upperLimit);
   CombPT_condCut[ 1] = new TH1D("Comb, vL_C_HB"  , "Very Low pT, Central eta, High Brems - 2/32"    , nBins, lowerLimit, upperLimit);
   CombPT_condCut[ 2] = new TH1D("Comb, vL_M_LB"  , "Very Low pT, Medium eta, Low Brems - 3/32"      , nBins, lowerLimit, upperLimit);
   CombPT_condCut[ 3] = new TH1D("Comb, vL_M_HB"  , "Very Low pT, Medium eta, High Brems - 4/32"     , nBins, lowerLimit, upperLimit);
   CombPT_condCut[ 4] = new TH1D("Comb, vL_Cr_LB" , "Very Low pT, Crack eta, Low Brems - 5/32"       , nBins, lowerLimit, upperLimit);
   CombPT_condCut[ 5] = new TH1D("Comb, vL_Cr_HB" , "Very Low pT, Crack eta, High Brems - 6/32"      , nBins, lowerLimit, upperLimit);
   CombPT_condCut[ 6] = new TH1D("Comb, vL_F_LB"  , "Very Low pT, Forward eta, Low Brems - 7/32"     , nBins, lowerLimit, upperLimit);
   CombPT_condCut[ 7] = new TH1D("Comb, vL_F_HB"  , "Very Low pT, Forward eta, High Brems - 8/32"    , nBins, lowerLimit, upperLimit);

   CombPT_condCut[ 8] = new TH1D("Comb, L_C_LB"   , "Low pT, Central eta, Low Brems - 9/32"  , nBins, lowerLimit, upperLimit);
   CombPT_condCut[ 9] = new TH1D("Comb, L_C_HB"   , "Low pT, Central eta, High Brems - 10/32", nBins, lowerLimit, upperLimit);
   CombPT_condCut[10] = new TH1D("Comb, L_M_LB"   , "Low pT, Medium eta, Low Brems - 11/32"  , nBins, lowerLimit, upperLimit);
   CombPT_condCut[11] = new TH1D("Comb, L_M_HB"   , "Low pT, Medium eta, High Brems - 12/32" , nBins, lowerLimit, upperLimit);
   CombPT_condCut[12] = new TH1D("Comb, L_Cr_LB"  , "Low pT, Crack eta, Low Brems - 13/32"   , nBins, lowerLimit, upperLimit);
   CombPT_condCut[13] = new TH1D("Comb, L_Cr_HB"  , "Low pT, Crack eta, High Brems - 14/32"  , nBins, lowerLimit, upperLimit);
   CombPT_condCut[14] = new TH1D("Comb, L_F_LB"   , "Low pT, Forward eta, Low Brems - 15/32" , nBins, lowerLimit, upperLimit);
   CombPT_condCut[15] = new TH1D("Comb, L_F_HB"   , "Low pT, Forward eta, High Brems - 16/32", nBins, lowerLimit, upperLimit);

   CombPT_condCut[16] = new TH1D("Comb, M_C_LB"   , "Medium pT Central eta, Low Brems - 17/32"   , nBins, lowerLimit, upperLimit);
   CombPT_condCut[17] = new TH1D("Comb, M_C_HB"   , "Medium pT Central eta, High Brems - 18/32"  , nBins, lowerLimit, upperLimit);
   CombPT_condCut[18] = new TH1D("Comb, M_M_LB"   , "Medium pT Medium eta, Low Brems - 19/32"    , nBins, lowerLimit, upperLimit);
   CombPT_condCut[19] = new TH1D("Comb, M_M_HB"   , "Medium pT Medium eta, High Brems - 20/32"   , nBins, lowerLimit, upperLimit);
   CombPT_condCut[20] = new TH1D("Comb, M_Cr_LB"  , "Medium pT Crack eta, Low Brems - 21/32"     , nBins, lowerLimit, upperLimit);
   CombPT_condCut[21] = new TH1D("Comb, M_Cr_HB"  , "Medium pT Crack eta, High Brems - 22/32"    , nBins, lowerLimit, upperLimit);
   CombPT_condCut[22] = new TH1D("Comb, M_F_LB"   , "Medium pT Forward eta, Low Brems - 23/32"   , nBins, lowerLimit, upperLimit);
   CombPT_condCut[23] = new TH1D("Comb, M_F_HB"   , "Medium pT Forward eta, High Brems - 24/32"  , nBins, lowerLimit, upperLimit);

   CombPT_condCut[24] = new TH1D("Comb, H_C_LB"   , "High pT, Central eta, Low Brems - 25/32"   , nBins, lowerLimit, upperLimit);
   CombPT_condCut[25] = new TH1D("Comb, H_C_HB"   , "High pT, Central eta, High Brems - 26/32"  , nBins, lowerLimit, upperLimit);
   CombPT_condCut[26] = new TH1D("Comb, H_M_LB"   , "High pT, Medium eta, Low Brems - 27/32"    , nBins, lowerLimit, upperLimit);
   CombPT_condCut[27] = new TH1D("Comb, H_M_HB"   , "High pT, Medium eta, High Brems - 28/32"   , nBins, lowerLimit, upperLimit);
   CombPT_condCut[28] = new TH1D("Comb, H_Cr_LB"  , "High pT, Crack eta, Low Brems - 29/32"     , nBins, lowerLimit, upperLimit);
   CombPT_condCut[29] = new TH1D("Comb, H_Cr_HB"  , "High pT, Crack eta, High Brems - 30/32"    , nBins, lowerLimit, upperLimit);
   CombPT_condCut[30] = new TH1D("Comb, H_F_LB"   , "High pT, Forward eta, Low Brems - 31/32"   , nBins, lowerLimit, upperLimit);
   CombPT_condCut[31] = new TH1D("Comb, H_F_HB"   , "High pT, Forward eta, High Brems - 32/32"  , nBins, lowerLimit, upperLimit);



   // track, sorted by combined pT
   TrackPT_condComb[ 0] = new TH1D("Trk condComb, vL_C_LB"  , "Very Low pT, Central eta, Low Brems - 1/32"     , nBins, lowerLimit, upperLimit);
   TrackPT_condComb[ 1] = new TH1D("Trk condComb, vL_C_HB"  , "Very Low pT, Central eta, High Brems - 2/32"    , nBins, lowerLimit, upperLimit);
   TrackPT_condComb[ 2] = new TH1D("Trk condComb, vL_M_LB"  , "Very Low pT, Medium eta, Low Brems - 3/32"      , nBins, lowerLimit, upperLimit);
   TrackPT_condComb[ 3] = new TH1D("Trk condComb, vL_M_HB"  , "Very Low pT, Medium eta, High Brems - 4/32"     , nBins, lowerLimit, upperLimit);
   TrackPT_condComb[ 4] = new TH1D("Trk condComb, vL_Cr_LB" , "Very Low pT, Crack eta, Low Brems - 5/32"       , nBins, lowerLimit, upperLimit);
   TrackPT_condComb[ 5] = new TH1D("Trk condComb, vL_Cr_HB" , "Very Low pT, Crack eta, High Brems - 6/32"      , nBins, lowerLimit, upperLimit);
   TrackPT_condComb[ 6] = new TH1D("Trk condComb, vL_F_LB"  , "Very Low pT, Forward eta, Low Brems - 7/32"     , nBins, lowerLimit, upperLimit);
   TrackPT_condComb[ 7] = new TH1D("Trk condComb, vL_F_HB"  , "Very Low pT, Forward eta, High Brems - 8/32"    , nBins, lowerLimit, upperLimit);

   TrackPT_condComb[ 8] = new TH1D("Trk condComb, L_C_LB"   , "Low pT, Central eta, Low Brems - 9/32"  , nBins, lowerLimit, upperLimit);
   TrackPT_condComb[ 9] = new TH1D("Trk condComb, L_C_HB"   , "Low pT, Central eta, High Brems - 10/32", nBins, lowerLimit, upperLimit);
   TrackPT_condComb[10] = new TH1D("Trk condComb, L_M_LB"   , "Low pT, Medium eta, Low Brems - 11/32"  , nBins, lowerLimit, upperLimit);
   TrackPT_condComb[11] = new TH1D("Trk condComb, L_M_HB"   , "Low pT, Medium eta, High Brems - 12/32" , nBins, lowerLimit, upperLimit);
   TrackPT_condComb[12] = new TH1D("Trk condComb, L_Cr_LB"  , "Low pT, Crack eta, Low Brems - 13/32"   , nBins, lowerLimit, upperLimit);
   TrackPT_condComb[13] = new TH1D("Trk condComb, L_Cr_HB"  , "Low pT, Crack eta, High Brems - 14/32"  , nBins, lowerLimit, upperLimit);
   TrackPT_condComb[14] = new TH1D("Trk condComb, L_F_LB"   , "Low pT, Forward eta, Low Brems - 15/32" , nBins, lowerLimit, upperLimit);
   TrackPT_condComb[15] = new TH1D("Trk condComb, L_F_HB"   , "Low pT, Forward eta, High Brems - 16/32", nBins, lowerLimit, upperLimit);

   TrackPT_condComb[16] = new TH1D("Trk condComb, M_C_LB"   , "Medium pT Central eta, Low Brems - 17/32"   , nBins, lowerLimit, upperLimit);
   TrackPT_condComb[17] = new TH1D("Trk condComb, M_C_HB"   , "Medium pT Central eta, High Brems - 18/32"  , nBins, lowerLimit, upperLimit);
   TrackPT_condComb[18] = new TH1D("Trk condComb, M_M_LB"   , "Medium pT Medium eta, Low Brems - 19/32"    , nBins, lowerLimit, upperLimit);
   TrackPT_condComb[19] = new TH1D("Trk condComb, M_M_HB"   , "Medium pT Medium eta, High Brems - 20/32"   , nBins, lowerLimit, upperLimit);
   TrackPT_condComb[20] = new TH1D("Trk condComb, M_Cr_LB"  , "Medium pT Crack eta, Low Brems - 21/32"     , nBins, lowerLimit, upperLimit);
   TrackPT_condComb[21] = new TH1D("Trk condComb, M_Cr_HB"  , "Medium pT Crack eta, High Brems - 22/32"    , nBins, lowerLimit, upperLimit);
   TrackPT_condComb[22] = new TH1D("Trk condComb, M_F_LB"   , "Medium pT Forward eta, Low Brems - 23/32"   , nBins, lowerLimit, upperLimit);
   TrackPT_condComb[23] = new TH1D("Trk condComb, M_F_HB"   , "Medium pT Forward eta, High Brems - 24/32"  , nBins, lowerLimit, upperLimit);

   TrackPT_condComb[24] = new TH1D("Trk condComb, H_C_LB"   , "High pT, Central eta, Low Brems - 25/32"   , nBins, lowerLimit, upperLimit);
   TrackPT_condComb[25] = new TH1D("Trk condComb, H_C_HB"   , "High pT, Central eta, High Brems - 26/32"  , nBins, lowerLimit, upperLimit);
   TrackPT_condComb[26] = new TH1D("Trk condComb, H_M_LB"   , "High pT, Medium eta, Low Brems - 27/32"    , nBins, lowerLimit, upperLimit);
   TrackPT_condComb[27] = new TH1D("Trk condComb, H_M_HB"   , "High pT, Medium eta, High Brems - 28/32"   , nBins, lowerLimit, upperLimit);
   TrackPT_condComb[28] = new TH1D("Trk condComb, H_Cr_LB"  , "High pT, Crack eta, Low Brems - 29/32"     , nBins, lowerLimit, upperLimit);
   TrackPT_condComb[29] = new TH1D("Trk condComb, H_Cr_HB"  , "High pT, Crack eta, High Brems - 30/32"    , nBins, lowerLimit, upperLimit);
   TrackPT_condComb[30] = new TH1D("Trk condComb, H_F_LB"   , "High pT, Forward eta, Low Brems - 31/32"   , nBins, lowerLimit, upperLimit);
   TrackPT_condComb[31] = new TH1D("Trk condComb, H_F_HB"   , "High pT, Forward eta, High Brems - 32/32"  , nBins, lowerLimit, upperLimit);


   // calorimeter, sorted by combined pT
   CaloPT_condComb[ 0] = new TH1D("Calo condComb, vL_C_LB"  , "Very Low pT, Central eta, Low Brems - 1/32"     , nBins, lowerLimit, upperLimit);
   CaloPT_condComb[ 1] = new TH1D("Calo condComb, vL_C_HB"  , "Very Low pT, Central eta, High Brems - 2/32"    , nBins, lowerLimit, upperLimit);
   CaloPT_condComb[ 2] = new TH1D("Calo condComb, vL_M_LB"  , "Very Low pT, Medium eta, Low Brems - 3/32"      , nBins, lowerLimit, upperLimit);
   CaloPT_condComb[ 3] = new TH1D("Calo condComb, vL_M_HB"  , "Very Low pT, Medium eta, High Brems - 4/32"     , nBins, lowerLimit, upperLimit);
   CaloPT_condComb[ 4] = new TH1D("Calo condComb, vL_Cr_LB" , "Very Low pT, Crack eta, Low Brems - 5/32"       , nBins, lowerLimit, upperLimit);
   CaloPT_condComb[ 5] = new TH1D("Calo condComb, vL_Cr_HB" , "Very Low pT, Crack eta, High Brems - 6/32"      , nBins, lowerLimit, upperLimit);
   CaloPT_condComb[ 6] = new TH1D("Calo condComb, vL_F_LB"  , "Very Low pT, Forward eta, Low Brems - 7/32"     , nBins, lowerLimit, upperLimit);
   CaloPT_condComb[ 7] = new TH1D("Calo condComb, vL_F_HB"  , "Very Low pT, Forward eta, High Brems - 8/32"    , nBins, lowerLimit, upperLimit);

   CaloPT_condComb[ 8] = new TH1D("Calo condComb, L_C_LB"   , "Low pT, Central eta, Low Brems - 9/32"  , nBins, lowerLimit, upperLimit);
   CaloPT_condComb[ 9] = new TH1D("Calo condComb, L_C_HB"   , "Low pT, Central eta, High Brems - 10/32", nBins, lowerLimit, upperLimit);
   CaloPT_condComb[10] = new TH1D("Calo condComb, L_M_LB"   , "Low pT, Medium eta, Low Brems - 11/32"  , nBins, lowerLimit, upperLimit);
   CaloPT_condComb[11] = new TH1D("Calo condComb, L_M_HB"   , "Low pT, Medium eta, High Brems - 12/32" , nBins, lowerLimit, upperLimit);
   CaloPT_condComb[12] = new TH1D("Calo condComb, L_Cr_LB"  , "Low pT, Crack eta, Low Brems - 13/32"   , nBins, lowerLimit, upperLimit);
   CaloPT_condComb[13] = new TH1D("Calo condComb, L_Cr_HB"  , "Low pT, Crack eta, High Brems - 14/32"  , nBins, lowerLimit, upperLimit);
   CaloPT_condComb[14] = new TH1D("Calo condComb, L_F_LB"   , "Low pT, Forward eta, Low Brems - 15/32" , nBins, lowerLimit, upperLimit);
   CaloPT_condComb[15] = new TH1D("Calo condComb, L_F_HB"   , "Low pT, Forward eta, High Brems - 16/32", nBins, lowerLimit, upperLimit);

   CaloPT_condComb[16] = new TH1D("Calo condComb, M_C_LB"   , "Medium pT Central eta, Low Brems - 17/32"   , nBins, lowerLimit, upperLimit);
   CaloPT_condComb[17] = new TH1D("Calo condComb, M_C_HB"   , "Medium pT Central eta, High Brems - 18/32"  , nBins, lowerLimit, upperLimit);
   CaloPT_condComb[18] = new TH1D("Calo condComb, M_M_LB"   , "Medium pT Medium eta, Low Brems - 19/32"    , nBins, lowerLimit, upperLimit);
   CaloPT_condComb[19] = new TH1D("Calo condComb, M_M_HB"   , "Medium pT Medium eta, High Brems - 20/32"   , nBins, lowerLimit, upperLimit);
   CaloPT_condComb[20] = new TH1D("Calo condComb, M_Cr_LB"  , "Medium pT Crack eta, Low Brems - 21/32"     , nBins, lowerLimit, upperLimit);
   CaloPT_condComb[21] = new TH1D("Calo condComb, M_Cr_HB"  , "Medium pT Crack eta, High Brems - 22/32"    , nBins, lowerLimit, upperLimit);
   CaloPT_condComb[22] = new TH1D("Calo condComb, M_F_LB"   , "Medium pT Forward eta, Low Brems - 23/32"   , nBins, lowerLimit, upperLimit);
   CaloPT_condComb[23] = new TH1D("Calo condComb, M_F_HB"   , "Medium pT Forward eta, High Brems - 24/32"  , nBins, lowerLimit, upperLimit);

   CaloPT_condComb[24] = new TH1D("Calo condComb, H_C_LB"   , "High pT, Central eta, Low Brems - 25/32"   , nBins, lowerLimit, upperLimit);
   CaloPT_condComb[25] = new TH1D("Calo condComb, H_C_HB"   , "High pT, Central eta, High Brems - 26/32"  , nBins, lowerLimit, upperLimit);
   CaloPT_condComb[26] = new TH1D("Calo condComb, H_M_LB"   , "High pT, Medium eta, Low Brems - 27/32"    , nBins, lowerLimit, upperLimit);
   CaloPT_condComb[27] = new TH1D("Calo condComb, H_M_HB"   , "High pT, Medium eta, High Brems - 28/32"   , nBins, lowerLimit, upperLimit);
   CaloPT_condComb[28] = new TH1D("Calo condComb, H_Cr_LB"  , "High pT, Crack eta, Low Brems - 29/32"     , nBins, lowerLimit, upperLimit);
   CaloPT_condComb[29] = new TH1D("Calo condComb, H_Cr_HB"  , "High pT, Crack eta, High Brems - 30/32"    , nBins, lowerLimit, upperLimit);
   CaloPT_condComb[30] = new TH1D("Calo condComb, H_F_LB"   , "High pT, Forward eta, Low Brems - 31/32"   , nBins, lowerLimit, upperLimit);
   CaloPT_condComb[31] = new TH1D("Calo condComb, H_F_HB"   , "High pT, Forward eta, High Brems - 32/32"  , nBins, lowerLimit, upperLimit);



   // Calrometer or Combined pT: Combined if we do combination, 
   CaloOrCombPT_condNone[ 0] = new TH1D("CaloOrComb, vL_C_LB"  , "Very Low pT, Central eta, Low Brems - 1/32"     , nBins, lowerLimit, upperLimit);
   CaloOrCombPT_condNone[ 1] = new TH1D("CaloOrComb, vL_C_HB"  , "Very Low pT, Central eta, High Brems - 2/32"    , nBins, lowerLimit, upperLimit);
   CaloOrCombPT_condNone[ 2] = new TH1D("CaloOrComb, vL_M_LB"  , "Very Low pT, Medium eta, Low Brems - 3/32"      , nBins, lowerLimit, upperLimit);
   CaloOrCombPT_condNone[ 3] = new TH1D("CaloOrComb, vL_M_HB"  , "Very Low pT, Medium eta, High Brems - 4/32"     , nBins, lowerLimit, upperLimit);
   CaloOrCombPT_condNone[ 4] = new TH1D("CaloOrComb, vL_Cr_LB" , "Very Low pT, Crack eta, Low Brems - 5/32"       , nBins, lowerLimit, upperLimit);
   CaloOrCombPT_condNone[ 5] = new TH1D("CaloOrComb, vL_Cr_HB" , "Very Low pT, Crack eta, High Brems - 6/32"      , nBins, lowerLimit, upperLimit);
   CaloOrCombPT_condNone[ 6] = new TH1D("CaloOrComb, vL_F_LB"  , "Very Low pT, Forward eta, Low Brems - 7/32"     , nBins, lowerLimit, upperLimit);
   CaloOrCombPT_condNone[ 7] = new TH1D("CaloOrComb, vL_F_HB"  , "Very Low pT, Forward eta, High Brems - 8/32"    , nBins, lowerLimit, upperLimit);

   CaloOrCombPT_condNone[ 8] = new TH1D("CaloOrComb, L_C_LB"   , "Low pT, Central eta, Low Brems - 9/32"  , nBins, lowerLimit, upperLimit);
   CaloOrCombPT_condNone[ 9] = new TH1D("CaloOrComb, L_C_HB"   , "Low pT, Central eta, High Brems - 10/32", nBins, lowerLimit, upperLimit);
   CaloOrCombPT_condNone[10] = new TH1D("CaloOrComb, L_M_LB"   , "Low pT, Medium eta, Low Brems - 11/32"  , nBins, lowerLimit, upperLimit);
   CaloOrCombPT_condNone[11] = new TH1D("CaloOrComb, L_M_HB"   , "Low pT, Medium eta, High Brems - 12/32" , nBins, lowerLimit, upperLimit);
   CaloOrCombPT_condNone[12] = new TH1D("CaloOrComb, L_Cr_LB"  , "Low pT, Crack eta, Low Brems - 13/32"   , nBins, lowerLimit, upperLimit);
   CaloOrCombPT_condNone[13] = new TH1D("CaloOrComb, L_Cr_HB"  , "Low pT, Crack eta, High Brems - 14/32"  , nBins, lowerLimit, upperLimit);
   CaloOrCombPT_condNone[14] = new TH1D("CaloOrComb, L_F_LB"   , "Low pT, Forward eta, Low Brems - 15/32" , nBins, lowerLimit, upperLimit);
   CaloOrCombPT_condNone[15] = new TH1D("CaloOrComb, L_F_HB"   , "Low pT, Forward eta, High Brems - 16/32", nBins, lowerLimit, upperLimit);

   CaloOrCombPT_condNone[16] = new TH1D("CaloOrComb, M_C_LB"   , "Medium pT Central eta, Low Brems - 17/32"   , nBins, lowerLimit, upperLimit);
   CaloOrCombPT_condNone[17] = new TH1D("CaloOrComb, M_C_HB"   , "Medium pT Central eta, High Brems - 18/32"  , nBins, lowerLimit, upperLimit);
   CaloOrCombPT_condNone[18] = new TH1D("CaloOrComb, M_M_LB"   , "Medium pT Medium eta, Low Brems - 19/32"    , nBins, lowerLimit, upperLimit);
   CaloOrCombPT_condNone[19] = new TH1D("CaloOrComb, M_M_HB"   , "Medium pT Medium eta, High Brems - 20/32"   , nBins, lowerLimit, upperLimit);
   CaloOrCombPT_condNone[20] = new TH1D("CaloOrComb, M_Cr_LB"  , "Medium pT Crack eta, Low Brems - 21/32"     , nBins, lowerLimit, upperLimit);
   CaloOrCombPT_condNone[21] = new TH1D("CaloOrComb, M_Cr_HB"  , "Medium pT Crack eta, High Brems - 22/32"    , nBins, lowerLimit, upperLimit);
   CaloOrCombPT_condNone[22] = new TH1D("CaloOrComb, M_F_LB"   , "Medium pT Forward eta, Low Brems - 23/32"   , nBins, lowerLimit, upperLimit);
   CaloOrCombPT_condNone[23] = new TH1D("CaloOrComb, M_F_HB"   , "Medium pT Forward eta, High Brems - 24/32"  , nBins, lowerLimit, upperLimit);

   CaloOrCombPT_condNone[24] = new TH1D("CaloOrComb, H_C_LB"   , "High pT, Central eta, Low Brems - 25/32"   , nBins, lowerLimit, upperLimit);
   CaloOrCombPT_condNone[25] = new TH1D("CaloOrComb, H_C_HB"   , "High pT, Central eta, High Brems - 26/32"  , nBins, lowerLimit, upperLimit);
   CaloOrCombPT_condNone[26] = new TH1D("CaloOrComb, H_M_LB"   , "High pT, Medium eta, Low Brems - 27/32"    , nBins, lowerLimit, upperLimit);
   CaloOrCombPT_condNone[27] = new TH1D("CaloOrComb, H_M_HB"   , "High pT, Medium eta, High Brems - 28/32"   , nBins, lowerLimit, upperLimit);
   CaloOrCombPT_condNone[28] = new TH1D("CaloOrComb, H_Cr_LB"  , "High pT, Crack eta, Low Brems - 29/32"     , nBins, lowerLimit, upperLimit);
   CaloOrCombPT_condNone[29] = new TH1D("CaloOrComb, H_Cr_HB"  , "High pT, Crack eta, High Brems - 30/32"    , nBins, lowerLimit, upperLimit);
   CaloOrCombPT_condNone[30] = new TH1D("CaloOrComb, H_F_LB"   , "High pT, Forward eta, Low Brems - 31/32"   , nBins, lowerLimit, upperLimit);
   CaloOrCombPT_condNone[31] = new TH1D("CaloOrComb, H_F_HB"   , "High pT, Forward eta, High Brems - 32/32"  , nBins, lowerLimit, upperLimit);


   // Calrorimeter pT: Combined if we do combination, 
   Calo_condNone[ 0] = new TH1D("Calo condNone, vL_C_LB"  , "Very Low pT, Central eta, Low Brems - 1/32"     , nBins, lowerLimit, upperLimit);
   Calo_condNone[ 1] = new TH1D("Calo condNone, vL_C_HB"  , "Very Low pT, Central eta, High Brems - 2/32"    , nBins, lowerLimit, upperLimit);
   Calo_condNone[ 2] = new TH1D("Calo condNone, vL_M_LB"  , "Very Low pT, Medium eta, Low Brems - 3/32"      , nBins, lowerLimit, upperLimit);
   Calo_condNone[ 3] = new TH1D("Calo condNone, vL_M_HB"  , "Very Low pT, Medium eta, High Brems - 4/32"     , nBins, lowerLimit, upperLimit);
   Calo_condNone[ 4] = new TH1D("Calo condNone, vL_Cr_LB" , "Very Low pT, Crack eta, Low Brems - 5/32"       , nBins, lowerLimit, upperLimit);
   Calo_condNone[ 5] = new TH1D("Calo condNone, vL_Cr_HB" , "Very Low pT, Crack eta, High Brems - 6/32"      , nBins, lowerLimit, upperLimit);
   Calo_condNone[ 6] = new TH1D("Calo condNone, vL_F_LB"  , "Very Low pT, Forward eta, Low Brems - 7/32"     , nBins, lowerLimit, upperLimit);
   Calo_condNone[ 7] = new TH1D("Calo condNone, vL_F_HB"  , "Very Low pT, Forward eta, High Brems - 8/32"    , nBins, lowerLimit, upperLimit);

   Calo_condNone[ 8] = new TH1D("Calo condNone, L_C_LB"   , "Low pT, Central eta, Low Brems - 9/32"  , nBins, lowerLimit, upperLimit);
   Calo_condNone[ 9] = new TH1D("Calo condNone, L_C_HB"   , "Low pT, Central eta, High Brems - 10/32", nBins, lowerLimit, upperLimit);
   Calo_condNone[10] = new TH1D("Calo condNone, L_M_LB"   , "Low pT, Medium eta, Low Brems - 11/32"  , nBins, lowerLimit, upperLimit);
   Calo_condNone[11] = new TH1D("Calo condNone, L_M_HB"   , "Low pT, Medium eta, High Brems - 12/32" , nBins, lowerLimit, upperLimit);
   Calo_condNone[12] = new TH1D("Calo condNone, L_Cr_LB"  , "Low pT, Crack eta, Low Brems - 13/32"   , nBins, lowerLimit, upperLimit);
   Calo_condNone[13] = new TH1D("Calo condNone, L_Cr_HB"  , "Low pT, Crack eta, High Brems - 14/32"  , nBins, lowerLimit, upperLimit);
   Calo_condNone[14] = new TH1D("Calo condNone, L_F_LB"   , "Low pT, Forward eta, Low Brems - 15/32" , nBins, lowerLimit, upperLimit);
   Calo_condNone[15] = new TH1D("Calo condNone, L_F_HB"   , "Low pT, Forward eta, High Brems - 16/32", nBins, lowerLimit, upperLimit);

   Calo_condNone[16] = new TH1D("Calo condNone, M_C_LB"   , "Medium pT Central eta, Low Brems - 17/32"   , nBins, lowerLimit, upperLimit);
   Calo_condNone[17] = new TH1D("Calo condNone, M_C_HB"   , "Medium pT Central eta, High Brems - 18/32"  , nBins, lowerLimit, upperLimit);
   Calo_condNone[18] = new TH1D("Calo condNone, M_M_LB"   , "Medium pT Medium eta, Low Brems - 19/32"    , nBins, lowerLimit, upperLimit);
   Calo_condNone[19] = new TH1D("Calo condNone, M_M_HB"   , "Medium pT Medium eta, High Brems - 20/32"   , nBins, lowerLimit, upperLimit);
   Calo_condNone[20] = new TH1D("Calo condNone, M_Cr_LB"  , "Medium pT Crack eta, Low Brems - 21/32"     , nBins, lowerLimit, upperLimit);
   Calo_condNone[21] = new TH1D("Calo condNone, M_Cr_HB"  , "Medium pT Crack eta, High Brems - 22/32"    , nBins, lowerLimit, upperLimit);
   Calo_condNone[22] = new TH1D("Calo condNone, M_F_LB"   , "Medium pT Forward eta, Low Brems - 23/32"   , nBins, lowerLimit, upperLimit);
   Calo_condNone[23] = new TH1D("Calo condNone, M_F_HB"   , "Medium pT Forward eta, High Brems - 24/32"  , nBins, lowerLimit, upperLimit);

   Calo_condNone[24] = new TH1D("Calo condNone, H_C_LB"   , "High pT, Central eta, Low Brems - 25/32"   , nBins, lowerLimit, upperLimit);
   Calo_condNone[25] = new TH1D("Calo condNone, H_C_HB"   , "High pT, Central eta, High Brems - 26/32"  , nBins, lowerLimit, upperLimit);
   Calo_condNone[26] = new TH1D("Calo condNone, H_M_LB"   , "High pT, Medium eta, Low Brems - 27/32"    , nBins, lowerLimit, upperLimit);
   Calo_condNone[27] = new TH1D("Calo condNone, H_M_HB"   , "High pT, Medium eta, High Brems - 28/32"   , nBins, lowerLimit, upperLimit);
   Calo_condNone[28] = new TH1D("Calo condNone, H_Cr_LB"  , "High pT, Crack eta, Low Brems - 29/32"     , nBins, lowerLimit, upperLimit);
   Calo_condNone[29] = new TH1D("Calo condNone, H_Cr_HB"  , "High pT, Crack eta, High Brems - 30/32"    , nBins, lowerLimit, upperLimit);
   Calo_condNone[30] = new TH1D("Calo condNone, H_F_LB"   , "High pT, Forward eta, Low Brems - 31/32"   , nBins, lowerLimit, upperLimit);
   Calo_condNone[31] = new TH1D("Calo condNone, H_F_HB"   , "High pT, Forward eta, High Brems - 32/32"  , nBins, lowerLimit, upperLimit);


   // Calrorimeter or Combined pT: Combined if we do combination, 
   Track_condNone[ 0] = new TH1D("Track condNone, vL_C_LB"  , "Very Low pT, Central eta, Low Brems - 1/32"     , nBins, lowerLimit, upperLimit);
   Track_condNone[ 1] = new TH1D("Track condNone, vL_C_HB"  , "Very Low pT, Central eta, High Brems - 2/32"    , nBins, lowerLimit, upperLimit);
   Track_condNone[ 2] = new TH1D("Track condNone, vL_M_LB"  , "Very Low pT, Medium eta, Low Brems - 3/32"      , nBins, lowerLimit, upperLimit);
   Track_condNone[ 3] = new TH1D("Track condNone, vL_M_HB"  , "Very Low pT, Medium eta, High Brems - 4/32"     , nBins, lowerLimit, upperLimit);
   Track_condNone[ 4] = new TH1D("Track condNone, vL_Cr_LB" , "Very Low pT, Crack eta, Low Brems - 5/32"       , nBins, lowerLimit, upperLimit);
   Track_condNone[ 5] = new TH1D("Track condNone, vL_Cr_HB" , "Very Low pT, Crack eta, High Brems - 6/32"      , nBins, lowerLimit, upperLimit);
   Track_condNone[ 6] = new TH1D("Track condNone, vL_F_LB"  , "Very Low pT, Forward eta, Low Brems - 7/32"     , nBins, lowerLimit, upperLimit);
   Track_condNone[ 7] = new TH1D("Track condNone, vL_F_HB"  , "Very Low pT, Forward eta, High Brems - 8/32"    , nBins, lowerLimit, upperLimit);

   Track_condNone[ 8] = new TH1D("Track condNone, L_C_LB"   , "Low pT, Central eta, Low Brems - 9/32"  , nBins, lowerLimit, upperLimit);
   Track_condNone[ 9] = new TH1D("Track condNone, L_C_HB"   , "Low pT, Central eta, High Brems - 10/32", nBins, lowerLimit, upperLimit);
   Track_condNone[10] = new TH1D("Track condNone, L_M_LB"   , "Low pT, Medium eta, Low Brems - 11/32"  , nBins, lowerLimit, upperLimit);
   Track_condNone[11] = new TH1D("Track condNone, L_M_HB"   , "Low pT, Medium eta, High Brems - 12/32" , nBins, lowerLimit, upperLimit);
   Track_condNone[12] = new TH1D("Track condNone, L_Cr_LB"  , "Low pT, Crack eta, Low Brems - 13/32"   , nBins, lowerLimit, upperLimit);
   Track_condNone[13] = new TH1D("Track condNone, L_Cr_HB"  , "Low pT, Crack eta, High Brems - 14/32"  , nBins, lowerLimit, upperLimit);
   Track_condNone[14] = new TH1D("Track condNone, L_F_LB"   , "Low pT, Forward eta, Low Brems - 15/32" , nBins, lowerLimit, upperLimit);
   Track_condNone[15] = new TH1D("Track condNone, L_F_HB"   , "Low pT, Forward eta, High Brems - 16/32", nBins, lowerLimit, upperLimit);

   Track_condNone[16] = new TH1D("Track condNone, M_C_LB"   , "Medium pT Central eta, Low Brems - 17/32"   , nBins, lowerLimit, upperLimit);
   Track_condNone[17] = new TH1D("Track condNone, M_C_HB"   , "Medium pT Central eta, High Brems - 18/32"  , nBins, lowerLimit, upperLimit);
   Track_condNone[18] = new TH1D("Track condNone, M_M_LB"   , "Medium pT Medium eta, Low Brems - 19/32"    , nBins, lowerLimit, upperLimit);
   Track_condNone[19] = new TH1D("Track condNone, M_M_HB"   , "Medium pT Medium eta, High Brems - 20/32"   , nBins, lowerLimit, upperLimit);
   Track_condNone[20] = new TH1D("Track condNone, M_Cr_LB"  , "Medium pT Crack eta, Low Brems - 21/32"     , nBins, lowerLimit, upperLimit);
   Track_condNone[21] = new TH1D("Track condNone, M_Cr_HB"  , "Medium pT Crack eta, High Brems - 22/32"    , nBins, lowerLimit, upperLimit);
   Track_condNone[22] = new TH1D("Track condNone, M_F_LB"   , "Medium pT Forward eta, Low Brems - 23/32"   , nBins, lowerLimit, upperLimit);
   Track_condNone[23] = new TH1D("Track condNone, M_F_HB"   , "Medium pT Forward eta, High Brems - 24/32"  , nBins, lowerLimit, upperLimit);

   Track_condNone[24] = new TH1D("Track condNone, H_C_LB"   , "High pT, Central eta, Low Brems - 25/32"   , nBins, lowerLimit, upperLimit);
   Track_condNone[25] = new TH1D("Track condNone, H_C_HB"   , "High pT, Central eta, High Brems - 26/32"  , nBins, lowerLimit, upperLimit);
   Track_condNone[26] = new TH1D("Track condNone, H_M_LB"   , "High pT, Medium eta, Low Brems - 27/32"    , nBins, lowerLimit, upperLimit);
   Track_condNone[27] = new TH1D("Track condNone, H_M_HB"   , "High pT, Medium eta, High Brems - 28/32"   , nBins, lowerLimit, upperLimit);
   Track_condNone[28] = new TH1D("Track condNone, H_Cr_LB"  , "High pT, Crack eta, Low Brems - 29/32"     , nBins, lowerLimit, upperLimit);
   Track_condNone[29] = new TH1D("Track condNone, H_Cr_HB"  , "High pT, Crack eta, High Brems - 30/32"    , nBins, lowerLimit, upperLimit);
   Track_condNone[30] = new TH1D("Track condNone, H_F_LB"   , "High pT, Forward eta, Low Brems - 31/32"   , nBins, lowerLimit, upperLimit);
   Track_condNone[31] = new TH1D("Track condNone, H_F_HB"   , "High pT, Forward eta, High Brems - 32/32"  , nBins, lowerLimit, upperLimit);


   // Calrorimeter pT: Combined if we do combination, 
   Calo_condCaloOrComb[ 0] = new TH1D("Calo condCaloOrComb, vL_C_LB"  , "Very Low pT, Central eta, Low Brems - 1/32"     , nBins, lowerLimit, upperLimit);
   Calo_condCaloOrComb[ 1] = new TH1D("Calo condCaloOrComb, vL_C_HB"  , "Very Low pT, Central eta, High Brems - 2/32"    , nBins, lowerLimit, upperLimit);
   Calo_condCaloOrComb[ 2] = new TH1D("Calo condCaloOrComb, vL_M_LB"  , "Very Low pT, Medium eta, Low Brems - 3/32"      , nBins, lowerLimit, upperLimit);
   Calo_condCaloOrComb[ 3] = new TH1D("Calo condCaloOrComb, vL_M_HB"  , "Very Low pT, Medium eta, High Brems - 4/32"     , nBins, lowerLimit, upperLimit);
   Calo_condCaloOrComb[ 4] = new TH1D("Calo condCaloOrComb, vL_Cr_LB" , "Very Low pT, Crack eta, Low Brems - 5/32"       , nBins, lowerLimit, upperLimit);
   Calo_condCaloOrComb[ 5] = new TH1D("Calo condCaloOrComb, vL_Cr_HB" , "Very Low pT, Crack eta, High Brems - 6/32"      , nBins, lowerLimit, upperLimit);
   Calo_condCaloOrComb[ 6] = new TH1D("Calo condCaloOrComb, vL_F_LB"  , "Very Low pT, Forward eta, Low Brems - 7/32"     , nBins, lowerLimit, upperLimit);
   Calo_condCaloOrComb[ 7] = new TH1D("Calo condCaloOrComb, vL_F_HB"  , "Very Low pT, Forward eta, High Brems - 8/32"    , nBins, lowerLimit, upperLimit);

   Calo_condCaloOrComb[ 8] = new TH1D("Calo condCaloOrComb, L_C_LB"   , "Low pT, Central eta, Low Brems - 9/32"  , nBins, lowerLimit, upperLimit);
   Calo_condCaloOrComb[ 9] = new TH1D("Calo condCaloOrComb, L_C_HB"   , "Low pT, Central eta, High Brems - 10/32", nBins, lowerLimit, upperLimit);
   Calo_condCaloOrComb[10] = new TH1D("Calo condCaloOrComb, L_M_LB"   , "Low pT, Medium eta, Low Brems - 11/32"  , nBins, lowerLimit, upperLimit);
   Calo_condCaloOrComb[11] = new TH1D("Calo condCaloOrComb, L_M_HB"   , "Low pT, Medium eta, High Brems - 12/32" , nBins, lowerLimit, upperLimit);
   Calo_condCaloOrComb[12] = new TH1D("Calo condCaloOrComb, L_Cr_LB"  , "Low pT, Crack eta, Low Brems - 13/32"   , nBins, lowerLimit, upperLimit);
   Calo_condCaloOrComb[13] = new TH1D("Calo condCaloOrComb, L_Cr_HB"  , "Low pT, Crack eta, High Brems - 14/32"  , nBins, lowerLimit, upperLimit);
   Calo_condCaloOrComb[14] = new TH1D("Calo condCaloOrComb, L_F_LB"   , "Low pT, Forward eta, Low Brems - 15/32" , nBins, lowerLimit, upperLimit);
   Calo_condCaloOrComb[15] = new TH1D("Calo condCaloOrComb, L_F_HB"   , "Low pT, Forward eta, High Brems - 16/32", nBins, lowerLimit, upperLimit);

   Calo_condCaloOrComb[16] = new TH1D("Calo condCaloOrComb, M_C_LB"   , "Medium pT Central eta, Low Brems - 17/32"   , nBins, lowerLimit, upperLimit);
   Calo_condCaloOrComb[17] = new TH1D("Calo condCaloOrComb, M_C_HB"   , "Medium pT Central eta, High Brems - 18/32"  , nBins, lowerLimit, upperLimit);
   Calo_condCaloOrComb[18] = new TH1D("Calo condCaloOrComb, M_M_LB"   , "Medium pT Medium eta, Low Brems - 19/32"    , nBins, lowerLimit, upperLimit);
   Calo_condCaloOrComb[19] = new TH1D("Calo condCaloOrComb, M_M_HB"   , "Medium pT Medium eta, High Brems - 20/32"   , nBins, lowerLimit, upperLimit);
   Calo_condCaloOrComb[20] = new TH1D("Calo condCaloOrComb, M_Cr_LB"  , "Medium pT Crack eta, Low Brems - 21/32"     , nBins, lowerLimit, upperLimit);
   Calo_condCaloOrComb[21] = new TH1D("Calo condCaloOrComb, M_Cr_HB"  , "Medium pT Crack eta, High Brems - 22/32"    , nBins, lowerLimit, upperLimit);
   Calo_condCaloOrComb[22] = new TH1D("Calo condCaloOrComb, M_F_LB"   , "Medium pT Forward eta, Low Brems - 23/32"   , nBins, lowerLimit, upperLimit);
   Calo_condCaloOrComb[23] = new TH1D("Calo condCaloOrComb, M_F_HB"   , "Medium pT Forward eta, High Brems - 24/32"  , nBins, lowerLimit, upperLimit);

   Calo_condCaloOrComb[24] = new TH1D("Calo condCaloOrComb, H_C_LB"   , "High pT, Central eta, Low Brems - 25/32"   , nBins, lowerLimit, upperLimit);
   Calo_condCaloOrComb[25] = new TH1D("Calo condCaloOrComb, H_C_HB"   , "High pT, Central eta, High Brems - 26/32"  , nBins, lowerLimit, upperLimit);
   Calo_condCaloOrComb[26] = new TH1D("Calo condCaloOrComb, H_M_LB"   , "High pT, Medium eta, Low Brems - 27/32"    , nBins, lowerLimit, upperLimit);
   Calo_condCaloOrComb[27] = new TH1D("Calo condCaloOrComb, H_M_HB"   , "High pT, Medium eta, High Brems - 28/32"   , nBins, lowerLimit, upperLimit);
   Calo_condCaloOrComb[28] = new TH1D("Calo condCaloOrComb, H_Cr_LB"  , "High pT, Crack eta, Low Brems - 29/32"     , nBins, lowerLimit, upperLimit);
   Calo_condCaloOrComb[29] = new TH1D("Calo condCaloOrComb, H_Cr_HB"  , "High pT, Crack eta, High Brems - 30/32"    , nBins, lowerLimit, upperLimit);
   Calo_condCaloOrComb[30] = new TH1D("Calo condCaloOrComb, H_F_LB"   , "High pT, Forward eta, Low Brems - 31/32"   , nBins, lowerLimit, upperLimit);
   Calo_condCaloOrComb[31] = new TH1D("Calo condCaloOrComb, H_F_HB"   , "High pT, Forward eta, High Brems - 32/32"  , nBins, lowerLimit, upperLimit);


   // Calrorimeter or Combined pT: Combined if we do combination, 
   Track_condCaloOrComb[ 0] = new TH1D("Track condCaloOrComb, vL_C_LB"  , "Very Low pT, Central eta, Low Brems - 1/32"     , nBins, lowerLimit, upperLimit);
   Track_condCaloOrComb[ 1] = new TH1D("Track condCaloOrComb, vL_C_HB"  , "Very Low pT, Central eta, High Brems - 2/32"    , nBins, lowerLimit, upperLimit);
   Track_condCaloOrComb[ 2] = new TH1D("Track condCaloOrComb, vL_M_LB"  , "Very Low pT, Medium eta, Low Brems - 3/32"      , nBins, lowerLimit, upperLimit);
   Track_condCaloOrComb[ 3] = new TH1D("Track condCaloOrComb, vL_M_HB"  , "Very Low pT, Medium eta, High Brems - 4/32"     , nBins, lowerLimit, upperLimit);
   Track_condCaloOrComb[ 4] = new TH1D("Track condCaloOrComb, vL_Cr_LB" , "Very Low pT, Crack eta, Low Brems - 5/32"       , nBins, lowerLimit, upperLimit);
   Track_condCaloOrComb[ 5] = new TH1D("Track condCaloOrComb, vL_Cr_HB" , "Very Low pT, Crack eta, High Brems - 6/32"      , nBins, lowerLimit, upperLimit);
   Track_condCaloOrComb[ 6] = new TH1D("Track condCaloOrComb, vL_F_LB"  , "Very Low pT, Forward eta, Low Brems - 7/32"     , nBins, lowerLimit, upperLimit);
   Track_condCaloOrComb[ 7] = new TH1D("Track condCaloOrComb, vL_F_HB"  , "Very Low pT, Forward eta, High Brems - 8/32"    , nBins, lowerLimit, upperLimit);

   Track_condCaloOrComb[ 8] = new TH1D("Track condCaloOrComb, L_C_LB"   , "Low pT, Central eta, Low Brems - 9/32"  , nBins, lowerLimit, upperLimit);
   Track_condCaloOrComb[ 9] = new TH1D("Track condCaloOrComb, L_C_HB"   , "Low pT, Central eta, High Brems - 10/32", nBins, lowerLimit, upperLimit);
   Track_condCaloOrComb[10] = new TH1D("Track condCaloOrComb, L_M_LB"   , "Low pT, Medium eta, Low Brems - 11/32"  , nBins, lowerLimit, upperLimit);
   Track_condCaloOrComb[11] = new TH1D("Track condCaloOrComb, L_M_HB"   , "Low pT, Medium eta, High Brems - 12/32" , nBins, lowerLimit, upperLimit);
   Track_condCaloOrComb[12] = new TH1D("Track condCaloOrComb, L_Cr_LB"  , "Low pT, Crack eta, Low Brems - 13/32"   , nBins, lowerLimit, upperLimit);
   Track_condCaloOrComb[13] = new TH1D("Track condCaloOrComb, L_Cr_HB"  , "Low pT, Crack eta, High Brems - 14/32"  , nBins, lowerLimit, upperLimit);
   Track_condCaloOrComb[14] = new TH1D("Track condCaloOrComb, L_F_LB"   , "Low pT, Forward eta, Low Brems - 15/32" , nBins, lowerLimit, upperLimit);
   Track_condCaloOrComb[15] = new TH1D("Track condCaloOrComb, L_F_HB"   , "Low pT, Forward eta, High Brems - 16/32", nBins, lowerLimit, upperLimit);

   Track_condCaloOrComb[16] = new TH1D("Track condCaloOrComb, M_C_LB"   , "Medium pT Central eta, Low Brems - 17/32"   , nBins, lowerLimit, upperLimit);
   Track_condCaloOrComb[17] = new TH1D("Track condCaloOrComb, M_C_HB"   , "Medium pT Central eta, High Brems - 18/32"  , nBins, lowerLimit, upperLimit);
   Track_condCaloOrComb[18] = new TH1D("Track condCaloOrComb, M_M_LB"   , "Medium pT Medium eta, Low Brems - 19/32"    , nBins, lowerLimit, upperLimit);
   Track_condCaloOrComb[19] = new TH1D("Track condCaloOrComb, M_M_HB"   , "Medium pT Medium eta, High Brems - 20/32"   , nBins, lowerLimit, upperLimit);
   Track_condCaloOrComb[20] = new TH1D("Track condCaloOrComb, M_Cr_LB"  , "Medium pT Crack eta, Low Brems - 21/32"     , nBins, lowerLimit, upperLimit);
   Track_condCaloOrComb[21] = new TH1D("Track condCaloOrComb, M_Cr_HB"  , "Medium pT Crack eta, High Brems - 22/32"    , nBins, lowerLimit, upperLimit);
   Track_condCaloOrComb[22] = new TH1D("Track condCaloOrComb, M_F_LB"   , "Medium pT Forward eta, Low Brems - 23/32"   , nBins, lowerLimit, upperLimit);
   Track_condCaloOrComb[23] = new TH1D("Track condCaloOrComb, M_F_HB"   , "Medium pT Forward eta, High Brems - 24/32"  , nBins, lowerLimit, upperLimit);

   Track_condCaloOrComb[24] = new TH1D("Track condCaloOrComb, H_C_LB"   , "High pT, Central eta, Low Brems - 25/32"   , nBins, lowerLimit, upperLimit);
   Track_condCaloOrComb[25] = new TH1D("Track condCaloOrComb, H_C_HB"   , "High pT, Central eta, High Brems - 26/32"  , nBins, lowerLimit, upperLimit);
   Track_condCaloOrComb[26] = new TH1D("Track condCaloOrComb, H_M_LB"   , "High pT, Medium eta, Low Brems - 27/32"    , nBins, lowerLimit, upperLimit);
   Track_condCaloOrComb[27] = new TH1D("Track condCaloOrComb, H_M_HB"   , "High pT, Medium eta, High Brems - 28/32"   , nBins, lowerLimit, upperLimit);
   Track_condCaloOrComb[28] = new TH1D("Track condCaloOrComb, H_Cr_LB"  , "High pT, Crack eta, Low Brems - 29/32"     , nBins, lowerLimit, upperLimit);
   Track_condCaloOrComb[29] = new TH1D("Track condCaloOrComb, H_Cr_HB"  , "High pT, Crack eta, High Brems - 30/32"    , nBins, lowerLimit, upperLimit);
   Track_condCaloOrComb[30] = new TH1D("Track condCaloOrComb, H_F_LB"   , "High pT, Forward eta, Low Brems - 31/32"   , nBins, lowerLimit, upperLimit);
   Track_condCaloOrComb[31] = new TH1D("Track condCaloOrComb, H_F_HB"   , "High pT, Forward eta, High Brems - 32/32"  , nBins, lowerLimit, upperLimit);

   
   return;
}
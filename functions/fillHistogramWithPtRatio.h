// I wanna fit a crystal ball function to a distribution of PtPtTrueRatio, where Pt has either been measured by the tracker of the calorimeter
// there are different regions in pt, eta, and fbrem (defined as fabs(eLastMeasP-eMomentumTracker)/eMomentumTracker ) that I wanna fit seperately
// so I have for each combination of pt, eta, and fbrem regions a seperate TH1D histogram object where I store the PtPtTrueRatio ratios
// this function funnels the PtPtTrueRatio into the correct TH1D object dependent on pt, eta, and fbrem


#include <iostream>
#include "TH1D.h"

// assume from here on that the unit of momentum here is _MeV_
void fillHistogramWithPtRatio(double &fBrem, double &Eta, double &Pt,double &PtPtTrueRatio, TH1D* arrayTH1D[24], bool &m_useDebugMessages){

  // check for a number of exclusion criteria
     if( fBrem > 1 ||  fBrem < 0 )
        { if (m_useDebugMessages) {std::cout << "WARNING: Require fbrem [0,1], ignore this event! fbrem was: "<< fBrem << std::endl;}
          return;
        } // if( fBrem > 1 ||  fBrem < 0 )

      // check that the measured pT is within range
      if(Pt<5000. )
        { if(m_useDebugMessages){ std::cout << "WARNING: Require Pt > 5000 MeV, ignore this event! Pt was: "<< Pt << std::endl;}                                                    
          return;
        }//if(Pt<7000)

      // check that the pseudorapidity eta is within range
      if(fabs(Eta) > 2.5)
        { if(m_useDebugMessages){ std::cout << "WARNING: Require Eta<2.5, ignore this event! Eta was: "<< Eta << std::endl;}
          return;
        }// if(Eta > 2.5)


  // now the variables should be good. Funnel my pT ratios in the right histograms
  // we structure the funneling in this way: check for pt, then for eta, then for fBrem

    if(Pt<=7000.) // Low pT
      {   if(fabs(Eta)<=0.7) // Central Eta
          {   
            if(fBrem <= 0.2){ arrayTH1D[0]->Fill(PtPtTrueRatio);return; } // Low Brems
            else{             arrayTH1D[1]->Fill(PtPtTrueRatio);return; } // High Brems
          }             
          else if(fabs(Eta)<=1.37) // Medium Eta
          {
            if(fBrem <= 0.3){ arrayTH1D[2]->Fill(PtPtTrueRatio);return; } // Low Brems
            else{             arrayTH1D[3]->Fill(PtPtTrueRatio);return; } // High Brems
          }
          else if(fabs(Eta)<=1.52) // Creck Eta
          {         
            if(fBrem <= 0.3){ arrayTH1D[4]->Fill(PtPtTrueRatio);return; } // Low Brems
            else{             arrayTH1D[5]->Fill(PtPtTrueRatio);return; } // High Brems
          }
          else if(fabs(Eta)<=2.5) // Forward eta
          {
            if(fBrem <= 0.3){ arrayTH1D[6]->Fill(PtPtTrueRatio);return; } // Low Brems
            else{             arrayTH1D[7]->Fill(PtPtTrueRatio);return; } // High Brems
          }
            
      }//if(Pt<=15000.)

      else  if(Pt<=15000.) // Low pT
      {   if(fabs(Eta)<=0.7) // Central Eta
          {   
            if(fBrem <= 0.2){ arrayTH1D[8]->Fill(PtPtTrueRatio);return; } // Low Brems
            else{             arrayTH1D[9]->Fill(PtPtTrueRatio);return; } // High Brems
          }
          else if(fabs(Eta)<=1.37) // Medium Eta
          {
            if(fBrem <= 0.3){ arrayTH1D[10]->Fill(PtPtTrueRatio);return; } // Low Brems
            else{             arrayTH1D[11]->Fill(PtPtTrueRatio);return; } // High Brems
          }
          else if(fabs(Eta)<=1.52) // Creck Eta
          {         
            if(fBrem <= 0.3){ arrayTH1D[12]->Fill(PtPtTrueRatio);return; } // Low Brems
            else{             arrayTH1D[13]->Fill(PtPtTrueRatio);return; } // High Brems
          }
          else if(fabs(Eta)<=2.5) // Forward eta
          {
            if(fBrem <= 0.3){ arrayTH1D[14]->Fill(PtPtTrueRatio);return; } // Low Brems
            else{             arrayTH1D[15]->Fill(PtPtTrueRatio);return; } // High Brems
          }
            
      }//if(Pt<=15000.)


      else  if(Pt<=30000.) // medium pT
      {   if(fabs(Eta)<=0.7) // Central Eta
          {   
            if(fBrem <= 0.2){ arrayTH1D[16]->Fill(PtPtTrueRatio);return; } // Low Brems
            else{             arrayTH1D[17]->Fill(PtPtTrueRatio);return; } // High Brems
          }
          else if(fabs(Eta)<=1.37) // Medium Eta
          {
            if(fBrem <= 0.3){ arrayTH1D[18]->Fill(PtPtTrueRatio);return; } // Low Brems
            else{             arrayTH1D[19]->Fill(PtPtTrueRatio);return; } // High Brems
          }
          else if(fabs(Eta)<=1.52) // Creck Eta
          {         
            if(fBrem <= 0.3){ arrayTH1D[20]->Fill(PtPtTrueRatio);return; } // Low Brems
            else{             arrayTH1D[21]->Fill(PtPtTrueRatio);return; } // High Brems
          }
          else if(fabs(Eta)<=2.5) // Forward eta
          {
            if(fBrem <= 0.3){ arrayTH1D[22]->Fill(PtPtTrueRatio);return; } // Low Brems
            else{             arrayTH1D[23]->Fill(PtPtTrueRatio);return; } // High Brems
          }
            
      }//if(Pt<=30000.)
      else  // high pT
      {   if(fabs(Eta)<=0.7) // Central Eta
          {   
            if(fBrem <= 0.2){ arrayTH1D[24]->Fill(PtPtTrueRatio);return; } // Low Brems
            else{             arrayTH1D[25]->Fill(PtPtTrueRatio);return; } // High Brems
          }
          else if(fabs(Eta)<=1.37) // Medium Eta
          {
            if(fBrem <= 0.3){ arrayTH1D[26]->Fill(PtPtTrueRatio);return; } // Low Brems
            else{             arrayTH1D[27]->Fill(PtPtTrueRatio);return; } // High Brems
          }
          else if(fabs(Eta)<=1.52) // Creck Eta
          {         
            if(fBrem <= 0.3){ arrayTH1D[28]->Fill(PtPtTrueRatio);return; } // Low Brems
            else{             arrayTH1D[29]->Fill(PtPtTrueRatio);return; } // High Brems
          }
          else if(fabs(Eta)<=2.5) // Forward eta
          {
            if(fBrem <= 0.3){ arrayTH1D[30]->Fill(PtPtTrueRatio);return; } // Low Brems
            else{             arrayTH1D[31]->Fill(PtPtTrueRatio);return; } // High Brems
          }
            
      }// else // high pT

  if(true)
    { 
    std::cout << "SUPER WARNING! Something slipped through the cracks and did not fit in any cathegory"<<std::endl;
    std::cout << "fBrem = "<< fBrem << "; Eta = "<< Eta << "; Pt = "<< Pt << "; PtPtTrueRatio = "<< PtPtTrueRatio << "; fBrem = "<< fBrem <<std::endl;
    } //if(true){ 
return;
} // void fillHistogramWithPtRatio



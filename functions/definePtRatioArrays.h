// This is start of the header guard. By convention, we use the name of the header file.
#ifndef DEFINE_PT_RATIO_ARRAYS_H
#define DEFINE_PT_RATIO_ARRAYS_H

#include "TH1D.h"

// This is the content of the .h file, which is where the declarations go
        void defineArrays(TH1D* TrackPT_condCut[32], 
						  TH1D* CaloPT_condCut[32], 
						  TH1D* CombPT_condCut[32], 
						  TH1D* TrackPT_condComb[32], 
						  TH1D* CaloPT_condComb[32],
						  TH1D* CaloOrCombPT_condNone[32],
						  TH1D* Track_condNone[32],
                          TH1D* Calo_condCaloOrComb[32],
                  		  TH1D* Track_condCaloOrComb[32]); // function prototype for definePtRatioArrays.h -- don't forget the semicolon!

// This is the end of the header guard
#endif
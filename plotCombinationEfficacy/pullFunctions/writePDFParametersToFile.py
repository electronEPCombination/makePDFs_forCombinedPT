"""

writePDFParamtersToFile.py :


"""

# remember: to run this from the python prompt: execfile("writePDFParamtersToFile.py")

def writePDFParameters(rootFileName, histNr, caloParameters, trackParameters):

	phaseSpaceID = ["vLC_LB" ,"vLC_HB" ,"vLM_LB" ,"vLM_HB" ,"vLCr_LB" ,"vLCr_HB" ,"vLF_LB" ,"vLF_HB" ,
					"LC_LB"  ,"LC_HB"  ,"LM_LB"  ,"LM_HB"  ,"LCr_LB"  ,"LCr_HB"  ,"LF_LB"  ,"LF_HB"  ,
					"MC_LB"  ,"MC_HB"  ,"MM_LB"  ,"MM_HB"  ,"MCr_LB"  ,"MCr_HB"  ,"MF_LB"  ,"MF_HB"  ,
					"HC_LB"  ,"HC_HB"  ,"HM_LB"  ,"HM_HB"  ,"HCr_LB"  ,"HCr_HB"  ,"HF_LB"  ,"HF_HB"  ]


	if histNr == 0 : outputString = "//"+"\t"*10 + "meanCalo,\tmeanTrack,\tsigmaCalo,\tsigmaTrack,\taCalo,\t\taTrack,\t\tnCalo,\t\tnTrack,\t\tmeanGCalo,\tmeanGTrack,\tsigmaGCalo,\tsigmaGTrack,\tcaloCBWeight,\ttrackCBWeight,\n"
	else: outputString="";


	outputString += phaseSpaceID[histNr]

	outputString += "\t= new CombinedPT_calculator("

	for n in range(0,len(caloParameters)): outputString += "\t%f,\t%f," % (caloParameters[n],trackParameters[n])

	outputString +="\tm_useDebugMessages);\n"

	if histNr +1 == len(phaseSpaceID): outputString += "\n"

	text_file = open(rootFileName.replace(".root","_PDFOut.C"), "a") # a for append
	text_file.write(outputString)
	text_file.close()

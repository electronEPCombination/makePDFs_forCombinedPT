"""

pullDistParameters.py : [meanPullFit, sigmaPullFit] =  getPullDistParameters(pTRatioFrame)
Let's make a pull distribution from a RooPlot frame that contains one histogram plot and one fit to it.

"""

import ROOT

def getPullDistParameters(RootData, rootFileLabel, myFitCanvas = None,  rangeLow = .6, rangeHigh = 1.2):
	# suppress some of the ROOT messages
	ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.WARNING)

	"""
	if "02cut" in rootFileLabel:
		rangeLow  = 0.85
		rangeHigh = 1.2

	elif "095cut" in rootFileLabel : 
		rangeLow  = 0.6
		rangeHigh = 1.2
	else:
		rangeLow  = 0.5 + 0.025
		rangeHigh = 1.2
	"""
	rangeLow += .025

	if rangeHigh > 1.5: rangeHigh = 1.5

	# define my independent variable
	pTRatio  = ROOT.RooRealVar("pTRatio","pT/pT_true", rangeLow , rangeHigh);

	# if my object was a TH1D, transform it into the needed RooDataHist
	if isinstance(RootData,  ROOT.TH1D ):

		## add errors for the chi2 fits
		nBins = RootData.GetSize()
		for j in range(0,nBins): RootData.SetBinError(j,1)


		RooDataObject = ROOT.RooDataHist("RooDataObject","RooDataObject",  ROOT.RooArgList(pTRatio), RootData)

	else:
		RooDataObject = RootData

	# define fit parameters
	meanFit  = ROOT.RooRealVar("meanCalo","mean of gaussian",1,0.9,1.2);
	sigmaFit = ROOT.RooRealVar("sigmaCalo","width of gaussian",0.04,0,0.20);
	aFit = ROOT.RooRealVar("a","a of CB",0.96,-8,8);
	nFit = ROOT.RooRealVar("n","n of CB",.5,0,20);

	meanFitGauss  = ROOT.RooRealVar("meanGauss","mean of gaussian",1,0.9,1.2);
	sigmaFitGauss = ROOT.RooRealVar("sigmaGauss","width of gaussian",0.04,0,0.20);

	sumCoefficient = ROOT.RooRealVar("sumCoefficient", "sumCoefficient", 0.9,0.0,1.0)

	# define my model(s) 

	myCrystalBall = ROOT.RooCBShape("myCrystalBall","a Crystal Ball",pTRatio,meanFit,sigmaFit,aFit,nFit) ;
	myGauss = ROOT.RooGaussian("myGauss","a Gaussian", pTRatio, meanFitGauss, sigmaFitGauss) ;

	# adjust initial values for Tracker fits
	if "Tracker" in RootData.GetName(): 
		sigmaFit.setVal(0.4);
		sigmaFitGauss.setVal(0.4);
		meanFit.setVal(1.15)

	#Adjust Calorimeter High pT fit initial values
	if "Calorimeter" in RootData.GetName() and "High pT" in RootData.GetTitle():
		sigmaFit.setVal(0.02);
		sigmaFitGauss.setVal(0.02);

	#select my fit model

	if "onlyCB" in rootFileLabel: fitModelSelection = "CB" 
	else: fitModelSelection = "CB+Gauss"
	
	if fitModelSelection == "CB": 			modelFit = myCrystalBall
	elif fitModelSelection == "CB+Gauss":	modelFit = ROOT.RooAddPdf("pdfSum", "CB + Gaussian", myCrystalBall, myGauss, sumCoefficient )

		
	#perform a fit
	if "chi2" in rootFileLabel:

		
		myRangeSetting = ROOT.RooFit.Range(rangeLow , rangeHigh); 
		myWeightSetting = ROOT.RooFit.DataError(ROOT.RooAbsData.SumW2); 

		myLinkedList = ROOT.RooLinkedList()
		myLinkedList.Add(myRangeSetting)
		myLinkedList.Add(myWeightSetting)


		modelFit.chi2FitTo(RooDataObject,  myLinkedList); #ROOT.RooFit.DataError(ROOT.RooAbsData.SumW2)
	else:
		modelFit.fitTo(RooDataObject, ROOT.RooFit.Range(rangeLow , rangeHigh));

	# save the fit vallues in a list, so that we can hand them out of the function
	if fitModelSelection == "CB": 			fitModelParameters = [meanFit.getVal(), sigmaFit.getVal(), aFit.getVal(), nFit.getVal()]	
	elif fitModelSelection == "CB+Gauss":	fitModelParameters = [meanFit.getVal(), sigmaFit.getVal(), aFit.getVal(), nFit.getVal(), meanFitGauss.getVal(), sigmaFitGauss.getVal(), sumCoefficient.getVal()]
	


	# create my RooPlotFrame
	pTRatioFrame = pTRatio.frame(ROOT.RooFit.Title(RootData.GetTitle())); 
	pTRatioFrame.GetYaxis().SetLabelSize(.05)
	pTRatioFrame.GetYaxis().SetTitleSize(0.07)		
	pTRatioFrame.GetYaxis().SetTitleOffset(0.7)

	# plot the d#ata
	RooDataObject.plotOn(pTRatioFrame, ROOT.RooFit.Name("my data"));
	# plot the Crystal Ball now after the fit
	if "Calorimeter" in RootData.GetName(): modelFit.plotOn(pTRatioFrame,ROOT.RooFit.Name("fitted fit"),ROOT.RooFit.LineColor(ROOT.kRed));
	else: modelFit.plotOn(pTRatioFrame,ROOT.RooFit.Name("fitted fit"),ROOT.RooFit.LineColor(ROOT.kBlue));

	if isinstance(myFitCanvas, ROOT.TCanvas):
		# create a pad for the pT-ratio fits
		pad1 = ROOT.TPad("pad1", "pad1", 0, 0.55, 1, 1.0);
		pad1.SetBottomMargin(0.); # Upper and lower plot are joined
		pad1.SetGridx();          # Vertical grid
		pad1.Draw();              # Draw the upper pad: pad1
		pad1.cd();                # pad1 becomes the current pad

		# draw the frame
		pTRatioFrame.Draw()

		leg1a = ROOT.TLegend(0.15,0.78,0.35,0.88)
		leg1a.SetFillColor(ROOT.kWhite)
		leg1a.SetLineColor(ROOT.kWhite)
		leg1a.AddEntry("my data" ,"MC", "P");
		leg1a.AddEntry("fitted fit"   ,fitModelSelection ,"L" );
		leg1a.Draw();


		fitTextBox=ROOT.TPaveText(0.15,0.45,0.35,0.77,"NBNDC")#
		fitTextBox.SetFillColor(ROOT.kWhite)
		meanCBFitString = ("#mu_{CB} = %.5f" %(meanFit.getVal() ));  
	  	fitTextBox.AddText(meanCBFitString);
		sigmaCBFitString = ("#sigma_{CB} = %.5f" %(sigmaFit.getVal() ));  
	   	fitTextBox.AddText(sigmaCBFitString);
		aCBFitString = ("a_{CB} = %.5f" %(aFit.getVal() ));  
	  	fitTextBox.AddText(aCBFitString);
		nCBFitString = ("n_{CB} = %.5f" %(nFit.getVal() ));  
	   	fitTextBox.AddText(nCBFitString);

	   	if fitModelSelection == "CB+Gauss":
			meanGaussFitString = ("#mu_{Gauss} = %.5f" %(meanFitGauss.getVal() ) );  
	 	 	fitTextBox.AddText(meanGaussFitString);
			sigmaGaussFitString = ("#sigma_{Gauss} = %.5f" %(sigmaFitGauss.getVal() ));  
	 	  	fitTextBox.AddText(sigmaGaussFitString);
	   		sumCoeffString = ("weight_{CB} = %.5f" %(sumCoefficient.getVal() ));
	   		fitTextBox.AddText(sumCoeffString)

	   	fitTextBox.Draw();

	   	#myFitCanvas.Update()
 


		# switch back to the appropriate subcanvas
		if "Calorimeter" in RootData.GetName(): myFitCanvas.cd(1)
		else: myFitCanvas.cd(2)
	else: pad1=None; fitTextBox=None; leg1a=None;







	"""
	Derive the Pull values 
	"""



	# Get a histogram with the pulls of the data w.r.t to the fit
	thePullHist = pTRatioFrame.pullHist() ;

	#we wanna store the pull values in a RooDataSet, this requires to assoiate the RooDataSet with a RooRealVar
	#This is the RooRealVar that we use for that
	pullValue = ROOT.RooRealVar ("Pulls","Pulls",0);
	pullValue.setBins(40);
	#we store the pull values in this RooDataSet so we can fit them and plot their distribution
	pullDataSet = ROOT.RooDataSet("pullDataSet", "list of pull values", ROOT.RooArgSet(pullValue));

	# we need to read the values of the pulls out of the RooHist in which they are saved.
	# Specifically their values are the Y values in that histogram

	X = ROOT.Double(0); Y = ROOT.Double(0); # to store the X and Y values of the points in the histogram temprorarely
	thePullHistLength = thePullHist.GetN();

	for i in range(0,thePullHistLength):# iterate over all entries in the RooHist
	    
	    thePullHist.GetPoint(i,X,Y); # get the x and y coordinates of the i-th point
	    pullValue.setVal(Y);		    # save the y value as the pull value
	    pullDataSet.add(ROOT.RooArgSet(pullValue)); # and add it to the RooDataHist

	# define variables to hold the calculated mean and sgima
	meanPullCalc  =ROOT.RooRealVar("meanPullCalc","mean of Pull",0);
	sigmaPullCalc =ROOT.RooRealVar("sigmaPullCalc","sgima of Pull",1);
	#calculate the mean and sigma
	meanPullCalc.setVal( pullDataSet.mean(pullValue))
	sigmaPullCalc.setVal(pullDataSet.sigma(pullValue))

	mu = meanPullCalc.getVal()
	sigma = sigmaPullCalc.getVal()

	pullValue.setMin(mu - (4*sigma))
	pullValue.setMax(mu + (4*sigma))

	# Define the fit and fit parameters
	meanPullFit  =ROOT.RooRealVar("meanPullFit","mean of Pull", mu ,mu-(4*sigma), mu+(4*sigma));
	sigmaPullFit =ROOT.RooRealVar("sigmaPullFit","width of Pull",sigma,0,2*sigma);
	#define the fit
	pullFit = ROOT.RooGaussian("pullFit","fit to the Pull distribution",pullValue,meanPullFit,sigmaPullFit) ;
	# do the fit (should implicitly be an unbinned likelihood one)
	pullFit.fitTo(pullDataSet);    



	if isinstance(myFitCanvas, ROOT.TCanvas):

		"""
		Plot the Pulls
		"""

		xmin = ROOT.Double(); ymin = ROOT.Double(); xmax = ROOT.Double(); ymax = ROOT.Double();
		thePullHist.ComputeRange(xmin,ymin,xmax,ymax)

		# Make a pad to plot the pull distribution into
		pad2 = ROOT.TPad("pad2", "pad2", 0, 0.4, 1, 0.55);
		pad2.SetTopMargin(0.); # Upper and lower plot are joined
		pad2.SetBottomMargin(.3); # Upper and lower plot are joined
		pad2.SetGridx();          # Vertical grid
		pad2.Draw();              # Draw the upper pad: pad1  
		#pad2.DrawFrame(rangeLow,ymin,rangeHigh,ymax)
		pad2.cd();                # pad1 becomes the current pad

		
		#pTRatioFrameForPulls = pullValue.frame();#ROOT.RooFit.Title(RootData.GetTitle())); 
		#pullRooDataHist = ROOT.RooDataHist('pullRooDataHist','pullRooDataHist',thePullHist);
		#pullRooDataHist.plotOn(pTRatioFrameForPulls, ROOT.RooFit.Name("my pulls"));
		#pTRatioFrameForPulls.Draw();


		#pTRatioFrameForPulls = pTRatio.frame(ROOT.RooFit.Title(RootData.GetTitle())); 
		#pTRatioFrameForPulls.Draw()

		thePullHist.GetXaxis().SetRangeUser(rangeLow, rangeHigh);
		thePullHist.SetLineWidth(0)
		thePullHist.SetFillColor(ROOT.kRed)

		thePullHist.SetTitle("")

		thePullHist.GetXaxis().SetLabelSize(.15)
		thePullHist.GetXaxis().SetTitle(pTRatio.GetTitle() );
		thePullHist.GetXaxis().SetTitleSize(0.15)
		

		thePullHist.GetYaxis().SetLabelSize(.15)
		thePullHist.GetYaxis().SetTitle("Pulls");
		thePullHist.GetYaxis().SetTitleSize(0.15)		
		thePullHist.GetYaxis().SetTitleOffset(.15)

		if "Calorimeter" in RootData.GetName(): thePullHist.SetFillColor(ROOT.kRed);
		else: thePullHist.SetFillColor(ROOT.kBlue);

		thePullHist.Draw("AB");















		# switch back to the appropriate subcanvas
		if "Calorimeter" in RootData.GetName(): myFitCanvas.cd(1)
		else: myFitCanvas.cd(2)

		"""
		Plot the Pull distribution
		"""

		if "Calorimeter" in RootData.GetName(): tempPullTitle = "Calorimeter Pull Distribution"
		else: tempPullTitle = "Tracker Pull Distribution"

		# make a frame for plotting the pull distribution
		pullDistFrame = pullValue.frame(ROOT.RooFit.Title(tempPullTitle) )#, ROOT.RooFit.Range(-100,100))
		pullDistFrame.GetYaxis().SetLabelSize(.05)
		pullDistFrame.GetYaxis().SetTitleSize(0.06)		
		pullDistFrame.GetYaxis().SetTitleOffset(0.5)

		pullDistFrame.GetXaxis().SetLabelSize(.05)
		pullDistFrame.GetXaxis().SetTitleSize(0.06)		
		pullDistFrame.GetXaxis().SetTitleOffset(0.75)

		# plot pull distribution and pull fits
		pullDataSet.plotOn(pullDistFrame,ROOT.RooFit.Name("pull Distribution"))
		if "Calorimeter" in RootData.GetName(): pullFit.plotOn(pullDistFrame,ROOT.RooFit.Name("pull fit"),ROOT.RooFit.LineColor(ROOT.kRed));
		else: pullFit.plotOn(pullDistFrame,ROOT.RooFit.Name("pull fit"),ROOT.RooFit.LineColor(ROOT.kBlue));

		


		# Make a pad to plot the pull distribution into
		pad3 = ROOT.TPad("pad3", "pad3", 0, 0.0, 1, 0.4);
		#pad3.SetTopMargin(.15); # Upper and lower plot are joined
		pad3.SetGridx();          # Vertical grid
		pad3.Draw();              # Draw the upper pad: pad1
		pad3.cd();                # pad1 becomes the current pad


		#myFitCanvas = ROOT.TCanvas("myFitCanvas","TCanvas title",1280,720);
		#myFitCanvas.cd()
		pullDistFrame.Draw()

		leg1b = ROOT.TLegend(0.15,0.65,0.45,0.87)
		leg1b.SetFillColor(ROOT.kWhite)
		leg1b.SetLineColor(ROOT.kWhite)
		leg1b.AddEntry("pull Distribution" ,"Pull Distribution", "P");
		leg1b.AddEntry("pull fit"   ,"Gaussian" ,"L" );
		leg1b.Draw();


 
		meanFitString = ("#mu_{fit} = %.4f #pm %.4f" %(meanPullFit.getVal() , meanPullFit.getError()) );  
		stdFitString = ("#sigma_{fit} = %.4f #pm %.4f" %(sigmaPullFit.getVal(), sigmaPullFit.getError()) );  

		meanCalcString = ("#mu_{calc} = %.4f" %meanPullCalc.getVal()   );  
		stdCalcString = ("#sigma_{calc} = %.4f" %sigmaPullCalc.getVal());  

 
		pullText=ROOT.TPaveText(0.65,0.65,0.9,0.87,"NBNDC") 
		pullText.SetFillColor(ROOT.kWhite)
	  	pullText.AddText(meanFitString);
	   	pullText.AddText(stdFitString);
	  	pullText.AddText(meanCalcString);
	   	pullText.AddText(stdCalcString) ;

	   	pullText.Draw();

		myFitCanvas.Update()
	else: 
		# I am putting these variable out, so I better assign it something
		pullDistFrame = None
		myFitCanvas = None
		pullText = None
		leg1b = None
		pad2=None
		pad3=None
	#myFitCanvas.Draw();


	"""
	meanFit .Print()
	sigmaFit.Print()
	aFit 	.Print()
	nFit 	.Print()
	"""

	
#myFitCanvas->Print("outputPlots.pdf(",mySpecialTitleChar)

	return meanPullCalc, sigmaPullCalc, meanPullFit, sigmaPullFit, pTRatioFrame, pullDistFrame, pullDataSet, thePullHist, myFitCanvas, pullText, leg1b, pad1, pad2, pad3, fitTextBox, leg1a, fitModelParameters


#       execfile("plotFitPulls.py")
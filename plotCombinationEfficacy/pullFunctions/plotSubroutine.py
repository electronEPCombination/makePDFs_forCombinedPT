"""
plotSubroutine.py : plots the means and standard deviations, of things, depgending on input
"""


import itertools
import numpy as np

import matplotlib; matplotlib.use('Agg') ## 'Agg' allows me to use matplotlib without an xwindow
import matplotlib.pyplot as plt

def plotSubroutine(data1, data2, plotMetadata):
	

	# set of options for plotMetadata:
	# plotMetadata = { 
	# 'legendLabel' : 'Pulls',  
	# 'ylabel1' : 'Pull mean', 
	# 'ylabel2' : 'Pull standard deviation', 
	# 'plotTitle' : 'Pull overview, P/E $\in$ [0.5, 1.2]', 
	# 'plotFilename': 'combination_08_12limit_CB+Gauss',
	# 'ylim_1' : [-0.6, 0.6],
	# 'ylim_2' : [0, 13] 
	# 'show' : { 'Calo' : True, 'Track' : True, 'Comb' : True }
	# }



	#if 'ylim_1' not in plotMetadata.keys(): plotMetadata['ylim_1'] = [-0.6, 0.6]
	#if 'ylim_2' not in plotMetadata.keys(): plotMetadata['ylim_2'] = [0, 13]
	if 'show' 	not in plotMetadata.keys(): plotMetadata['show'] = { 'Calo' : True, 'Track' : True, 'Comb' : True }

	myPlotRange = range(0,32) # if I wanna make the plotrange variable

	# set the independent variable up so that there is some spacing between diferent pT groups
	x = range(1,8+1); 
	for n in range(0, len(myPlotRange)/8 -1 ): x.extend(range(x[-1]+2,x[-1]+2+8))
	x = np.asarray(x)

	# Create a figure of size 8x6 inches, 80 dots per inch
	myFig = plt.figure(figsize=(12, 6), dpi=150/1.5)


	"""""""""""""""""""""""""""
	# First Subplot
	"""""""""""""""""""""""""""
	# Create a new subplot from a grid of 1x1
	ax1 = plt.subplot(2, 1, 1)
	# set up a number of marker to cycle through
	myMarkerSet = ('d','h', 'o', 's', 'D',"v","p",'*','2',"<",'_','.')
	myMarker = itertools.cycle(myMarkerSet) 
	markerMeasure=14

	# do the plots
	if plotMetadata['show']['Comb']: 
		plt.plot(x[myPlotRange],data1['Comb'][myPlotRange], myMarker.next(),markersize=markerMeasure, markerfacecolor='g', alpha=0.9,label="Comb., " 	 + plotMetadata['legendLabel'])
		if markerMeasure > 8: markerMeasure-=2
	if plotMetadata['show']['Calo']: 
		plt.plot(x[myPlotRange],data1['Calo'][myPlotRange], myMarker.next(),markersize=markerMeasure, markerfacecolor='r', alpha=0.9,label="Calorimeter, "	 + plotMetadata['legendLabel']) 
		if markerMeasure > 8: markerMeasure -=2
	if plotMetadata['show']['Track']: 
		plt.plot(x[myPlotRange],data1['Track'][myPlotRange],myMarker.next(),markersize=markerMeasure, markerfacecolor='b', alpha=0.9,label="Tracker, " + plotMetadata['legendLabel']) 
		if markerMeasure > 8: markerMeasure -=2

	# remove the labels for the uppper subplot
	xTickLabels = ['']*len(myPlotRange)
	plt.xticks(x,xTickLabels)



	# add labels for the upper subplot
	plt.ylabel(plotMetadata['ylabel1'])
	plt.title(plotMetadata['plotTitle'])
	plt.grid()
	axes = plt.gca()
	axes.set_xlim([x[0]-1,x[myPlotRange[-1]]+1])
	if 'ylim_1' in plotMetadata.keys(): axes.set_ylim(plotMetadata['ylim_1'])

	"""""""""""""""""""""""""""
	# 2nd Subplot
	"""""""""""""""""""""""""""
	# Create the 2nd subplot from a grid of 1x1
	ax = plt.subplot(2, 1, 2)

	# adjust tge spacing of the subplot
	plt.subplots_adjust(bottom=0.33,right=0.7)


	myMarker = itertools.cycle(myMarkerSet) 
	markerMeasure=14

	# do the plots
#for l in range(0,len(rootFileList)):
	if plotMetadata['show']['Comb']: 
		plt.plot(x[myPlotRange],data2['Comb'][myPlotRange], myMarker.next(),markersize=markerMeasure,markerfacecolor='g', alpha=0.9,label="Comb., " 	 + plotMetadata['legendLabel'])
		if markerMeasure > 8: markerMeasure-=2
#for l in range(0,len(rootFileList)):
	if plotMetadata['show']['Calo']: 
		plt.plot(x[myPlotRange],data2['Calo'][myPlotRange], myMarker.next(),markersize=markerMeasure,markerfacecolor='r', alpha=0.9,label="Calorimeter "	 + plotMetadata['legendLabel']) 
		if markerMeasure > 8: markerMeasure -=2
#for l in range(0,len(rootFileList)):
	if plotMetadata['show']['Track']: 
		plt.plot(x[myPlotRange],data2['Track'][myPlotRange],myMarker.next(),markersize=markerMeasure,markerfacecolor='b', alpha=0.9,label="Tracker " + plotMetadata['legendLabel']) 
		if markerMeasure > 8: markerMeasure -=2

	# set lower xTick labels
	xTickLabels = [	'very Low PT, Central, LB', 'very Low PT, Central, HB',
					'very Low PT, Medium, LB',	'very Low PT, Medium, HB',
					'very Low PT, Crack, LB',	'very Low PT, Crack, HB',
					'very Low PT, Forward, LB',	'very Low PT, Forward, HB',
					'Low PT, Central, LB', 		'Low PT, Central, HB',
					'Low PT, Medium, LB',		'Low PT, Medium, HB',
					'Low PT, Crack, LB',		'Low PT, Crack, HB',
					'Low PT, Forward, LB',		'Low PT, Forward, HB',
					'Mid PT, Central, LB',		'Mid PT, Central, HB',
					'Mid PT, Medium, LB',		'Mid PT, Medium, HB',
					'Mid PT, Crack, LB',		'Mid PT, Crack, HB',
					'Mid PT, Forward, LB',		'Mid PT, Forward, HB',
					'High PT, Central, LB',		'High PT, Central, HB',
					'High PT, Medium, LB',		'High PT, Medium, HB',
					'High PT, Crack, LB',		'High PT, Crack, HB',
					'High PT, Forward, LB',		'High PT, Forward, HB']

	#and adjust them
	plt.xticks(x,xTickLabels,rotation='vertical',fontsize=11)


	# add labels to the 2nd subplot
	plt.ylabel(plotMetadata['ylabel2'])
	plt.legend(loc='lower right', bbox_to_anchor=(+1.52, +0.65),ncol=1,fontsize=11)
	plt.grid()
	axes = plt.gca()
	axes.set_xlim([x[0]-1,x[myPlotRange[-1]]+1])
	if 'ylim_2' in plotMetadata.keys(): axes.set_ylim(plotMetadata['ylim_2'] )#axes.set_ylim([0, 45])#
	

	#plt.show()

	plt.savefig(plotMetadata['plotFilename']+".png", dpi=300)
	#plt.savefig(plotMetadata['plotFilename']+".eps", dpi=300,format="eps")
	#plt.savefig(plotMetadata['plotFilename']+".svg", dpi=300,format="svg")
	plt.close()


	return True

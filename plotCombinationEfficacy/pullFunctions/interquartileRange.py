# provide the interquartile range of a TH1 object 
# (Interquartila range being the differense between the differense between th 75% quartile and the 25% quartile: Q75-Q25)

import numpy as np
import ROOT


def interquartileRange(theHistogram):

	nQuantilesToCalculate = 2;

	quantilePositions = np.array([0.25, 0.75]) #calculate the 25% and 75% quantiles

	quantileValues = np.zeros(nQuantilesToCalculate) #store the quantile values here

	theHistogram.GetQuantiles(nQuantilesToCalculate, quantileValues, quantilePositions) # get the quantiles from the histogram


	interquartileRange = quantileValues[1] - quantileValues[0]

	return interquartileRange
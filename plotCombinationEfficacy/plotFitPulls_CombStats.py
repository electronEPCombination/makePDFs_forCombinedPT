"""

plotFitPulls.py :


"""


# remember: to run this from the python prompt: 	execfile('plotFitPulls_CombStats.py')
# or alternatively run from bash via: 				python plotFitPulls_CombStats.py

import ROOT
import numpy as np
#import scipy as sp
import matplotlib; matplotlib.use('Agg') ## 'Agg' allows me to use matplotlib without an xwindow
import matplotlib.pyplot as plt
import copy
import itertools
from pullFunctions.pullDistParameters import getPullDistParameters
from pullFunctions.writePDFParametersToFile import writePDFParameters
from pullFunctions.plotSubroutine import plotSubroutine
from pullFunctions.interquartileRange import interquartileRange


# this is the list of .root files that I am interested in

#rootFileList = 	['combination_05_12limit_CB+Gauss.root']



rootFileList = 	["outAggregatedPlots_xAOD_calibratedEnergy_pileup.root"]




printPulls = True
printOverview = True
nHistograms = 32



thePlotTitle = []; lowerEPLimitList = []; upperEPLimitList = [];

# let's store my means and standard deviations in these lists of dicts of numpy arrays

myPullMeans = [{}] *len(rootFileList)
myPullStDevs = copy.deepcopy(myPullMeans)

myMeans = [{}] *len(rootFileList)
myStDevs = copy.deepcopy(myMeans)
myNEvents = copy.deepcopy(myMeans)
myInterquartileRange = copy.deepcopy(myMeans)

# for each of the files
for l in range(0,len(rootFileList)):
	
	tempPullMeans = {'Calo': np.zeros(nHistograms), 'Track': np.zeros(nHistograms),'Comb': np.zeros(nHistograms)}
	tempPullStDevs = copy.deepcopy(tempPullMeans)

	#tempMeans = {'Calo': np.zeros(24), 'Track': np.zeros(24),'Comb': np.zeros(24)}
	tempMeans = {'efficacy' : copy.deepcopy(tempPullMeans), 'effect' : copy.deepcopy(tempPullMeans)}
	tempStDevs = copy.deepcopy(tempMeans)
	tempNEvents = copy.deepcopy(tempMeans)
	tempInterquartileRange = copy.deepcopy(tempMeans)

	# open the current file
	aRootFile = ROOT.TFile(rootFileList[l],"READ")
	#Get the limits on P/E
	lowerEPLimit = copy.deepcopy( aRootFile.Get('LowerEPLimit') )
	upperEPLimit = copy.deepcopy( aRootFile.Get('UpperEPLimit') )

	lowerEPLimitList.append( lowerEPLimit.getVal() )
	upperEPLimitList.append( upperEPLimit.getVal() )
	#define the plot label (for the overview)

	thePlotTitle.append( ("Compare Pulls, P/E $\in$ [%.1f, %.1f]" %(lowerEPLimit.getVal(), upperEPLimit.getVal())) )

	# for every current file, go through each of the histograms
	
	for i in range(0,nHistograms):

		""" Get the combination statistics"""


		# We concider the pT/pT_true distributions for pT = pT_Calo, pT_Track, and pT_Comb, this list contains the three shorthands / dict keys for the three distributions
		combinationInstanceList = ['Calo', 'Track', 'Comb']
		# I have two ways to interpret the combination results:
		# Efficacy looks only at events that qualify for the EP combinaiton and compares the combined pT distributions to the track and calo of these events
		# Effect looks at all events, regardless whether they qualify for comparison or not, and compares the CaloOrComb pt (where the Combined pT is taken if the event qualifis for combination) with the calo and track pt
		# Fot this we need to look at different histograms, whole names are recorded in the python dict 'histNames'
		histNames = { 'efficacy' : {'Comb':'Combined','Calo':'Calorimeter','Track':'Tracker'}, 'effect': {'Comb':'CaloOrComb Unconditional','Calo':'Calorimeter conditional CaloOrComb','Track':'Tracker conditional CaloOrComb'} };

		interpretationOptions = ['efficacy' , 'effect' ]

		for interpretation in interpretationOptions:

			for combInstance in combinationInstanceList:
				# pick the i-th Calo/ Tracker or Comb histogram
				currentTH1D = copy.deepcopy( aRootFile.Get(histNames[interpretation][combInstance]+";%d" %(i+1)) ) 

				# save its mean and Stdandard Deviation
				tempMeans[interpretation][combInstance][i]   = np.copy( currentTH1D.GetMean(1)   )
				tempStDevs[interpretation][combInstance][i]  = np.copy( currentTH1D.GetStdDev(1) )
				tempNEvents[interpretation][combInstance][i] = np.copy( currentTH1D.GetSum() ) # The total number of events in the histogram
				tempInterquartileRange[interpretation][combInstance][i] = interquartileRange(currentTH1D) 

		""" Do the fits and pulls too by extension """
		if printPulls:
			# pick the i-th Calorimeter histograms
			currentTH1D = copy.deepcopy(aRootFile.Get("Calorimeter Fit Dist;%d" %(i+1)))
			# Add the "Calorimeter" to the title
			tempTitle = "Calorimeter "+currentTH1D.GetTitle()
			currentTH1D.SetTitle(tempTitle)

			if printPulls:
				savableFitCanvas = ROOT.TCanvas("fit+pulls;%d"%(i+1),"fit and pulls",1280,720);
				savableFitCanvas.Divide(2,1,0.01,0.01); 
				savableFitCanvas.cd(1)
				#myFitCanvas.DrawClonePad()
			else:
				savableFitCanvas = None


			meanPullCalc, sigmaPullCalc, meanPullFit, sigmaPullFit, pTRatioFrame, pullDistFrame, pullDataSet, thePullHist, myFitCanvas, pullTextCalo, leg1bCalo, pad1Calo, pad2Calo, pad3Calo, fitTextBoxCalo, leg1aCalo, caloParameters = getPullDistParameters(currentTH1D, rootFileList[l], savableFitCanvas, lowerEPLimit.getVal(), upperEPLimit.getVal() )
			

			# save its mean and Stdandard Deviation
			tempPullMeans['Calo'][i]   = np.copy(  meanPullCalc.getVal()  )
			tempPullStDevs['Calo'][i]  = np.copy(  sigmaPullCalc.getVal() )




			# pick the i-th Tracker histogram
			currentTH1D = copy.deepcopy(aRootFile.Get("Tracker Fit Dist;%d" %(i+1)))
			# Add the "Calorimeter" to the title
			tempTitle = "Tracker "+currentTH1D.GetTitle()
			currentTH1D.SetTitle(tempTitle)

			if printPulls:	savableFitCanvas.cd(2)

			meanPullCalc, sigmaPullCalc, meanPullFit, sigmaPullFit, pTRatioFrame, pullDistFrame, pullDataSet, thePullHist, myFitCanvas, pullTextTrack, leg1bTrack, pad1Tracker, pad2Tracker, pad3Tracker, fitTextBoxTrack, leg1aTrack, trackParameters = getPullDistParameters(currentTH1D, rootFileList[l], savableFitCanvas, lowerEPLimit.getVal(), upperEPLimit.getVal() )

			# save its mean and Stdandard Deviation
			tempPullMeans['Track'][i]  = np.copy( meanPullCalc.getVal()  )
			tempPullStDevs['Track'][i] = np.copy( sigmaPullCalc.getVal() )

			if printPulls:
				#savableFitCanvas.Write("fit+pulls;%d"%(i+1),ROOT.TObject.kOverwrite); # save the myFitCanvas to my .root file

				## For Saving Fit+Pulls as PDFs
				#set the file name for the pull distributions
				pullDistFileName = rootFileList[l].replace(".root","_Pull.pdf")
				if i < nHistograms-1: # it is not the last histogram in the TFile
					pullDistFileName += "("
				else:	# close the pdf if it is the last histogram
					pullDistFileName += ")"
				
				savableFitCanvas.Print(pullDistFileName)

			## write the PDF parameters from the fit to a file
			writePDFParameters(rootFileList[l], i , caloParameters, trackParameters)
			"""
			# pick the i-th Combined histogram
			currentTH1D = aRootFile.Get("Combined;%d" %(i+1))
			# save its mean and Stdandard Deviation
			tempPullMeans['Comb'][i]  = np.copy( currentTH1D.GetMean(1)   )
			tempPullStDevs['Comb'][i] = np.copy( currentTH1D.GetStdDev(1) )
			"""
	myPullMeans[l]  = tempPullMeans
	myPullStDevs[l] = tempPullStDevs

	myMeans[l]  = tempMeans
	myStDevs[l] = tempStDevs
	myNEvents[l] = tempNEvents
	myInterquartileRange[l] = tempInterquartileRange

	titleAddendum = ''

	# pull plots
	if printPulls:
		plotMetadata = { 'legendLabel' : 'Pulls',  'ylabel1' : 'Pull mean', 'ylabel2' : 'Pull standard deviation', 'plotTitle' :  ('Pull overview, P/E $\in$ [%.1f, %.1f]' %(lowerEPLimitList[l], upperEPLimitList[l]) ) + titleAddendum, 'plotFilename': rootFileList[l].replace(".root","_Pull"), 'show' : { 'Calo' : True, 'Track' : True, 'Comb' : False } }
		plotMetadata['ylim_1'] = [-0.6, 0.6]; plotMetadata['ylim_2'] = [0, 10];
		plotSubroutine(tempPullMeans, tempPullStDevs, plotMetadata)

	# efficacy plots
	plotMetadata = { 'legendLabel' : '',  'ylabel1' : 'mean of $pT/pT_{truth}$', 'ylabel2' : 'Std dev of $pT/pT_{truth}$', 'plotTitle' :  ('EP combination efficacy, P/E $\in$ [%.1f, %.1f]' %(lowerEPLimitList[l], upperEPLimitList[l]) + titleAddendum), 'plotFilename': rootFileList[l].replace(".root","_EFFICACY"), 'show' : { 'Calo' : True, 'Track' : True, 'Comb' : True } }
	plotMetadata['ylim_1'] = [.85 , 1.10]; plotMetadata['ylim_2'] = [.0 , 0.30]; 
	plotSubroutine(tempMeans['efficacy'], tempStDevs['efficacy'], plotMetadata)

	# efficacy plots (With interquartile range, instead of StdDev)
	plotMetadata = { 'legendLabel' : '',  'ylabel1' : 'mean of $pT/pT_{truth}$', 'ylabel2' : 'interquartile range \n of $pT/pT_{truth}$', 'plotTitle' :  ('EP combination efficacy, P/E $\in$ [%.1f, %.1f]' %(lowerEPLimitList[l], upperEPLimitList[l]) + titleAddendum), 'plotFilename': rootFileList[l].replace(".root","_EFFICACY_InterqartRange"), 'show' : { 'Calo' : True, 'Track' : True, 'Comb' : True } }
	plotMetadata['ylim_1'] = [.85 , 1.10]; plotMetadata['ylim_2'] = [.0 , 0.30]; 
	plotSubroutine(tempMeans['efficacy'], tempInterquartileRange['efficacy'], plotMetadata)

	# effect plots
	plotMetadata = { 'legendLabel' : '',  'ylabel1' : 'mean of $pT/pT_{truth}$', 'ylabel2' : 'Std dev of $pT/pT_{truth}$', 'plotTitle' :  ('EP combination effect, P/E $\in$ [%.1f, %.1f]' %(lowerEPLimitList[l], upperEPLimitList[l]) ) + titleAddendum, 'plotFilename': rootFileList[l].replace(".root","_EFFECT"), 'show' : { 'Calo' : True, 'Track' : True, 'Comb' : True } }
	plotMetadata['ylim_1'] = [.7 , 1.15]; plotMetadata['ylim_2'] = [.0 , 0.6]; 
	plotSubroutine(tempMeans['effect'], tempStDevs['effect'], plotMetadata)

	
	# calculate statistics and write them to a cvs file

	nEfficacyEvents = tempNEvents['efficacy']['Comb']
	nEffectEvents = tempNEvents['effect']['Comb']

	combinationCoverage = nEfficacyEvents / nEffectEvents
	
	combinationStdDevEfficacyEffectComparison = np.array([ tempStDevs['efficacy']['Calo'],	tempStDevs['efficacy']['Comb'], tempStDevs['efficacy']['Comb']/tempStDevs['efficacy']['Calo'], 
														   tempStDevs['effect']['Calo'], 	tempStDevs['effect']['Comb'], 	tempStDevs['effect']['Comb']/tempStDevs['effect']['Calo']      ] )

	cvsOutArray = np.array( [ ])

	coverageArray = np.array( [nEfficacyEvents, nEffectEvents, combinationCoverage] )

	np.savetxt(rootFileList[l].replace(".root","_COVERAGE.csv"), coverageArray.transpose() , delimiter=',')
	np.savetxt(rootFileList[l].replace(".root","_STDCompare.csv"), combinationStdDevEfficacyEffectComparison.transpose() , delimiter=',')

	# do a plot based on this statistic
	combinationCoverage = {'Comb': nEfficacyEvents / nEffectEvents }
	standardDevRatio = {'Comb': tempStDevs['effect']['Comb']/tempStDevs['effect']['Calo']  }
	plotMetadata = { 'legendLabel' : '',  'ylabel1' : 'Combination Coverage', 'ylabel2' : ' CaloOrCombined StdDevs / \n Calo StdDevs\n', 'plotTitle' :  ('Combination Statistics, P/E $\in$ [%.1f, %.1f]' %(lowerEPLimitList[l], upperEPLimitList[l]) ), 'plotFilename': rootFileList[l].replace(".root","_Stats"), 'show' : { 'Calo' : False, 'Track' : False, 'Comb' : True } }

	# Statistics plots
	if upperEPLimitList[l] <= 1.5: 
		plotMetadata['ylim_1'] = [.3 , 1.]; plotMetadata['ylim_2'] = [.95 , 1.02];
	plotSubroutine(combinationCoverage, standardDevRatio, plotMetadata)


#!/bin/bash

# This script is for Grace@Yale, no need to run this on LXPlus


. /gpfs/apps/hpc/Apps/ROOT/5.34.34/bin/thisroot.sh


module load Langs/Python

module load Libs/NUMPY ## load numpy module

module load Libs/SCIPY ## load scipy module

module load Libs/MATPLOTLIB


#lsetup ROOT ## setup root via the ATLAS stuff


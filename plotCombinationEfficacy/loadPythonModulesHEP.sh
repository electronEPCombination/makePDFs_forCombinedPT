#!/bin/bash

# This script is for HEP@Yale, no need to rung this on LXPlus
#cd makePDFs_forCombinedPT/myPyRootTest/


#module load Apps/ROOT/5.34.34

. /usr/local/cluster/hpc/Apps/ROOT/6.04.16/bin/thisroot.sh


module load Langs/Python

module load Libs/NUMPY ## load numpy module

module load Libs/SCIPY ## load scipy module

module load Libs/MATPLOTLIB


#lsetup ROOT ## setup root via the ATLAS stuff

## qsub -X -I -q hep_short -l walltime=4:00:00

